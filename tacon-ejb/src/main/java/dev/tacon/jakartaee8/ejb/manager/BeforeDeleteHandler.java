package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface BeforeDeleteHandler<E extends AbstractJpaEntity<?>> {

	/**
	 *
	 * @throws ManagerException
	 */
	default void deleteEntityNotFound() throws ManagerException {}

	void beforeDelete(@NonNull E entity) throws ManagerException;

}
