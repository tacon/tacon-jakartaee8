package dev.tacon.jakartaee8.ejb.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import dev.tacon.jakartaee8.dto.DtoWithID;
import dev.tacon.jakartaee8.ejb.eao.ApplicationEAO;
import dev.tacon.jakartaee8.ejb.manager.EntityFiller;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public class ManagerUtils {

	private ManagerUtils() {}

	public static <E extends AbstractJpaEntity<K>, D extends DtoWithID<D, K, ?>, K extends Serializable> void toEntityCollection(final Collection<E> entities, final Collection<D> dtos, final EntityManager em, final Supplier<E> newEntitySupplier, final BiConsumer<D, E> toEntity) {
		final Map<K, E> map = entities.stream().collect(Collectors.toMap(E::getId, Function.identity()));
		for (final D dto : dtos) {
			final K key = dto.getId();
			if (key == null) {
				final E e = newEntitySupplier.get();
				toEntity.accept(dto, e);
				entities.add(e);
				em.persist(e);
			} else {
				final E e = map.remove(key);
				if (e != null) {
					toEntity.accept(dto, e);
					em.merge(e);
				} else {
					// ERROR
				}
			}
		}
		for (final E e : map.values()) {
			entities.remove(e);
			em.remove(e);
		}
	}

	/**
	 * Given a collection of entities fetched from the persistence context
	 * and a collection of dto to save, merges the data inserting/updating
	 * the entities matching a common key and removing the unmatched entities.
	 *
	 * @param eao
	 * @param entities
	 * @param dtos
	 * @param entityKeyGetter
	 * @param dtoKeyGetter
	 * @param entitySupplier
	 * @param entityFiller
	 */
	public static <E, D, K> void merge(
			final ApplicationEAO eao,
			final Collection<E> entities,
			final Collection<D> dtos,
			final Function<E, K> entityKeyGetter,
			final Function<D, K> dtoKeyGetter,
			final Supplier<E> entitySupplier,
			final EntityFiller<K, D, E> entityFiller) {
		if (entities.isEmpty()) {
			for (final D dto : dtos) {
				final K key = dtoKeyGetter.apply(dto);
				final E entity = entitySupplier.get();
				entityFiller.fill(key, dto, entity);
				eao.insert(entity);
			}
			return;
		}
		final Map<K, E> entitiesMap = entities.stream().collect(Collectors.toMap(entityKeyGetter, Function.identity()));
		for (final D dto : dtos) {
			final K key = dtoKeyGetter.apply(dto);
			E entity = entitiesMap.remove(key);
			if (entity == null) {
				entity = entitySupplier.get();
				entityFiller.fill(key, dto, entity);
				eao.insert(entity);
			} else {
				entityFiller.fill(key, dto, entity);
				eao.update(entity);
			}
		}
		for (final E entity : entitiesMap.values()) {
			eao.delete(entity);
		}
	}
}
