package dev.tacon.jakartaee8.ejb.manager;

import java.util.List;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException.ValidationError;
import dev.tacon.jakartaee8.ejb.util.ValidationUtils;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface InsertHandler<E extends AbstractJpaEntity<?>, D>
		extends InsertValidator<D>, BeforeInsertHandler<E, D>, AfterInsertHandler<E, D> {

	@SuppressWarnings("rawtypes")
	InsertHandler NO_OP = new InsertHandler() {};

	@Override
	default void validateInsert(final @NonNull D dto) throws ManagerValidationException {
		final List<ValidationError> errors = ValidationUtils.validate(dto);
		if (!errors.isEmpty()) {
			throw new ManagerValidationException(errors);
		}
	}

	@Override
	default void beforeInsert(final @NonNull D dto, final @NonNull E entity) throws ManagerException {}

	@Override
	default void afterInsert(final @NonNull D dto, final @NonNull E entity) throws ManagerException {}
}
