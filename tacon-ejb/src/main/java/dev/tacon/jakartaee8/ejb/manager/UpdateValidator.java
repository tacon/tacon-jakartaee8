package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerConcurrencyException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;
import dev.tacon.jakartaee8.dto.ext.HasVersion;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface UpdateValidator<E extends AbstractJpaEntity<?>, D> {

	void validateUpdate(@NonNull D dto) throws ManagerValidationException;

	/**
	 * Checks the version of dto and entity comparing them.
	 * If their version does not match a {@link ManagerConcurrencyException} is thrown.
	 *
	 * <p>By default this method does the validation only if either dto and entity are
	 * {@link HasVersion} instances and their {@linkplain HasVersion#getVersion()}
	 * method returns a non-null value.</p>
	 *
	 * @param dto
	 * @param entity
	 * @throws ManagerConcurrencyException if the version does not match.
	 * @throws IncompatibileVersionTypeException if the return types of {@linkplain HasVersion#getVersion()} are incompatible.
	 */
	default void checkConcurrency(final @NonNull D dto, final @NonNull E entity) {
		ManagerInternals.checkVersion(dto, entity);
	}

	/**
	 * @param dto
	 * @param entity
	 * @throws ManagerValidationException
	 */
	default void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws ManagerValidationException {
		// NO-OP by default
	}

	/**
	 * Returns a new validator based on this instance which does not check
	 * for concurrency problems.
	 */
	default UpdateValidator<E, D> withoutConcurrencyCheck() {
		return new UpdateValidator<E, D>() {

			@Override
			public void validateUpdate(final @NonNull D dto) throws ManagerValidationException {
				UpdateValidator.this.validateUpdate(dto);
			}

			@Override
			public void checkConcurrency(final @NonNull D dto, final @NonNull E entity) {
				// prevent validation
			}

			@Override
			public void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws ManagerValidationException {
				UpdateValidator.this.validateAganistEntity(dto, entity);
			}
		};
	}
}
