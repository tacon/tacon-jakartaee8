package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public class InheritableInsertHandler<E extends AbstractJpaEntity<?>, D> implements InsertHandler<E, D> {

	private final InsertHandler<E, D> handler;

	public InheritableInsertHandler(final @NonNull InsertHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateInsert(final @NonNull D dto) throws ManagerValidationException {
		this.handler.validateInsert(dto);
	}

	@Override
	public void beforeInsert(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		this.handler.beforeInsert(dto, entity);
	}

	@Override
	public void afterInsert(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		this.handler.afterInsert(dto, entity);
	}

}
