package dev.tacon.jakartaee8.ejb.manager;

import java.io.Serializable;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public interface EntitySecurityManager<E extends AbstractJpaEntity<?>, P extends Serializable> {

	boolean hasReadAccess(@NonNull E entity);

	boolean hasWriteAccess(@NonNull E entity);

	@Nullable
	IPredicate prepareQueryFilter(@NonNull IRoot<E> root, @Nullable P params);

	static <E1 extends AbstractJpaEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> functional(
			final @NonNull Predicate<E1> readAccessTest,
			final @NonNull Predicate<E1> writeAccessTest,
			final @NonNull BiFunction<IRoot<E1>, P1, IPredicate> queryFilterFactory) {
		return new EntitySecurityManager<E1, P1>() {

			@Override
			public boolean hasReadAccess(final @NonNull E1 entity) {
				return readAccessTest.test(entity);
			}

			@Override
			public boolean hasWriteAccess(final @NonNull E1 entity) {
				return writeAccessTest.test(entity);
			}

			@Override
			public @Nullable IPredicate prepareQueryFilter(final @NonNull IRoot<E1> root, final @Nullable P1 params) {
				return queryFilterFactory.apply(root, params);
			}
		};
	}

	@SuppressWarnings("unchecked")
	static <E1 extends AbstractJpaEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> withoutSecurity() {
		return NO_SECURITY;
	}

	@SuppressWarnings("rawtypes")
	EntitySecurityManager NO_SECURITY = new EntitySecurityManager() {

		@Override
		public boolean hasReadAccess(final @NonNull AbstractJpaEntity entity) {
			return true;
		}

		@Override
		public boolean hasWriteAccess(final @NonNull AbstractJpaEntity entity) {
			return true;
		}

		@Override
		public IPredicate prepareQueryFilter(final @NonNull IRoot root, final @Nullable Serializable params) {
			return null;
		}
	};
}
