package dev.tacon.jakartaee8.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.ejb.eao.ApplicationEAO;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee8.persistence.query.Expressions;
import dev.tacon.jakartaee8.persistence.query.ISingleQuery;
import dev.tacon.jakartaee8.persistence.query.Queries;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public abstract class EntityBeanManager<E extends AbstractJpaEntity<I>, I extends Serializable, D, P extends Serializable> {

	private EntitySecurityManager<E, P> securityManager;

	public EntityBeanManager() {}

	public @NonNull D insert(final @NonNull D dto) throws ManagerException {
		final InsertHandler<E, D> handler = this.createInsertHandler(this.getEao());
		return this.toDto(this.insert(dto, handler, handler, handler));
	}

	public @NonNull E insert(final @NonNull D dto, final @NonNull InsertValidator<D> validator, final @NonNull BeforeInsertHandler<E, D> beforeInsertHandler, final @NonNull AfterInsertHandler<E, D> afterInsertHandler) throws ManagerException {
		validator.validateInsert(dto);
		final E entity = this.toEntity(dto);
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		beforeInsertHandler.beforeInsert(dto, entity);
		this.performInsert(entity);
		afterInsertHandler.afterInsert(dto, entity);
		return entity;
	}

	public void insert(final @NonNull E entity) throws ManagerException {
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		this.performInsert(entity);
	}

	/**
	 * @throws ManagerException
	 */
	protected void performInsert(final @NonNull E entity) throws ManagerException {
		this.getEao().insert(entity);
	}

	public void update(final @NonNull D dto) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, handler, handler, handler);
	}

	public D updateAndGet(final @NonNull D dto) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		final E updatedEntity = this.update(dto, handler, handler, handler);
		return updatedEntity != null ? this.toDto(updatedEntity) : null;
	}

	public E update(final @NonNull D dto, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		validator.validateUpdate(dto);
		final E entity = this.findEntity(this.toId(dto)).orElse(null);
		if (entity == null) {
			beforeUpdateHandler.updateEntityNotFound();
			return null;
		}
		return this.updateInternal(dto, entity, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	public void update(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, entity, handler, handler, handler);
	}

	public @NonNull E update(final @NonNull D dto, final @NonNull E entity, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		validator.validateUpdate(dto);
		return this.updateInternal(dto, entity, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	private @NonNull E updateInternal(final @NonNull D dto, final @NonNull E entity, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		validator.checkConcurrency(dto, entity);
		validator.validateAganistEntity(dto, entity);
		this.fillEntity(dto, entity);
		beforeUpdateHandler.beforeUpdate(dto, entity);
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		this.performUpdate(entity);
		afterUpdateHandler.afterUpdate(dto, entity);
		return entity;
	}

	public void update(final @NonNull E entity) throws ManagerException {
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		this.performUpdate(entity);
	}

	/**
	 * @param entity
	 * @throws ManagerException
	 */
	protected void performUpdate(final @NonNull E entity) throws ManagerException {
		this.getEao().update(entity);
	}

	public void delete(final @NonNull I id) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(id, handler, handler);
	}

	public E delete(final @NonNull I id, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		final Optional<E> optEntity = this.findEntity(id);
		if (!optEntity.isPresent()) {
			beforeDeleteHandler.deleteEntityNotFound();
			return null;
		}
		final @NonNull E entity = optEntity.get();
		this.delete(entity, beforeDeleteHandler, afterDeleteHandler);
		return entity;
	}

	public void delete(final @NonNull E entity, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		if (!this.getSecurityManager().hasWriteAccess(entity)) {
			throw new SecurityException();
		}
		beforeDeleteHandler.beforeDelete(entity);
		this.performDelete(entity);
		afterDeleteHandler.afterDelete(entity);
	}

	public void delete(final @NonNull E entity) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(entity, handler, handler);
	}

	/**
	 * @param entity
	 * @throws ManagerException
	 */
	protected void performDelete(final @NonNull E entity) throws ManagerException {
		this.getEao().delete(entity);
	}

	public @NonNull Optional<E> findEntity(final @Nullable I id) {
		if (id == null) {
			return Optional.empty();
		}
		final E entity = this.getEao().find(this.getEntityClass(), id);
		if (entity != null && !this.getSecurityManager().hasReadAccess(entity)) {
			throw new SecurityException();
		}
		return Optional.ofNullable(entity);
	}

	public D findDto(final @Nullable I id) {
		if (id == null) {
			return null;
		}
		return this.findEntity(id).map(this::toDto).orElse(null);
	}

	public E getEntityReference(final @Nullable I id) {
		return id != null ? this.getEao().getReference(this.getEntityClass(), id) : null;
	}

	protected abstract I toId(final @NonNull D dto);

	public @NonNull E toEntity(final @NonNull D dto) {
		final E entity = this.newEntity();
		this.fillEntityId(dto, entity);
		this.fillEntity(dto, entity);
		return entity;
	}

	protected @NonNull E newEntity() {
		try {
			return this.getEntityClass().getConstructor().newInstance();
		} catch (final ReflectiveOperationException ex) {
			throw new RuntimeException("Cannot find empty constructor for entity class: " + this.getEntityClass(), ex);
		}
	}

	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		entity.setId(this.toId(dto));
	}

	protected abstract void fillEntity(final @NonNull D dto, final @NonNull E entity);

	public @NonNull D toDto(final @NonNull E entity) {
		final D dto = this.newDto(entity);
		ManagerInternals.fillDtoVersion(dto, entity);
		this.fillDto(dto, entity);
		return dto;
	}

	protected abstract @NonNull D newDto(@NonNull E entity);

	protected abstract void fillDto(@NonNull D dto, @NonNull E entity);

	public @NonNull ISingleQuery<E, E> query(final P params) {
		final IRoot<E> root = Expressions.from(this.getEntityClass());
		return this.query(root, params);
	}

	public @NonNull ISingleQuery<E, E> query(final @NonNull IRoot<E> root, final P params) {
		final IPredicate queryFilter = this.getSecurityManager().prepareQueryFilter(root, params);
		final ISingleQuery<E, E> query = Queries.create(this.getEao().getEntityManager())
				.from(root).select(this.getEntityClass(), root);
		if (queryFilter != null) {
			return query.where(queryFilter);
		}
		return query;
	}

	protected abstract @NonNull EntitySecurityManager<E, P> createSecurityManager();

	protected final @NonNull EntitySecurityManager<E, P> getSecurityManager() {
		if (this.securityManager == null) {
			return this.securityManager = this.createSecurityManager();
		}
		return this.securityManager;
	}

	/**
	 * @param eao
	 * @return
	 */
	protected @NonNull @SuppressWarnings("unchecked") InsertHandler<E, D> createInsertHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof InsertHandler) {
			return (InsertHandler<E, D>) this;
		}
		return InsertHandler.NO_OP;
	}

	/**
	 * @param eao
	 * @return
	 */
	protected @NonNull @SuppressWarnings("unchecked") UpdateHandler<E, D> createUpdateHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof UpdateHandler) {
			return (UpdateHandler<E, D>) this;
		}
		return UpdateHandler.NO_OP;
	}

	/**
	 * @param eao
	 * @return
	 */
	protected @NonNull @SuppressWarnings("unchecked") DeleteHandler<E> createDeleteHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof DeleteHandler) {
			return (DeleteHandler<E>) this;
		}
		return DeleteHandler.NO_OP;
	}

	protected abstract @NonNull Class<E> getEntityClass();

	protected abstract @NonNull ApplicationEAO getEao();
}
