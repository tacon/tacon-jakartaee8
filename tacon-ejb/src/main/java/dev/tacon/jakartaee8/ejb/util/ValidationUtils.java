package dev.tacon.jakartaee8.ejb.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Path.Node;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import dev.tacon.annotations.NonNull;
import dev.tacon.beans.annotation.BeanProperty;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException.ValidationError;

public final class ValidationUtils {

	private static final ValidatorFactory VALIDATION_FACTORY = Validation.buildDefaultValidatorFactory();

	public static @NonNull <T> List<ValidationError> validate(final @NonNull T data) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final Set<ConstraintViolation<T>> violations = validator.validate(data);

		if (violations.isEmpty()) {
			return new ArrayList<>();
		}
		return violations.stream().map(ValidationUtils::ofConstraintViolation).collect(Collectors.toCollection(ArrayList::new));
	}

	public static @NonNull <T, V> List<ValidationError> validateValue(final @NonNull Class<T> dataClass, final @NonNull BeanProperty<?, ?, T, V> property, final V value) {
		final Validator validator = VALIDATION_FACTORY.getValidator();
		final String propertyName = property.getName();
		final Set<ConstraintViolation<T>> violations = validator.validateValue(dataClass, propertyName, value);

		if (violations.isEmpty()) {
			return new ArrayList<>();
		}
		return violations.stream().map(ValidationUtils::ofConstraintViolation).collect(Collectors.toCollection(ArrayList::new));
	}

	private static ValidationError ofConstraintViolation(final @NonNull ConstraintViolation<?> constraintViolation) {
		boolean field = false;
		final Path propertyPath = constraintViolation.getPropertyPath();
		final Object leafBean = constraintViolation.getLeafBean();
		if (leafBean != null) {
			Node last = null;
			for (final Node node : propertyPath) {
				last = node;
			}
			if (last != null) {
				field = isField(leafBean.getClass(), last.getName());
			}
		}

		final String messageTemplate = constraintViolation.getMessageTemplate();
		final Object invalidValue = constraintViolation.getInvalidValue();
		final Object rootBean = constraintViolation.getRootBean();

		return new ValidationError(
				constraintViolation.getConstraintDescriptor().getAnnotation(),
				rootBean instanceof Serializable ? (Serializable) rootBean : null,
				invalidValue instanceof Serializable ? (Serializable) invalidValue : null,
				field,
				propertyPath.toString(),
				constraintViolation.getMessage(),
				messageTemplate.startsWith("{") && messageTemplate.endsWith("}") ? messageTemplate.substring(1, messageTemplate.length() - 1) : null,
				constraintViolation.getConstraintDescriptor().getAttributes().entrySet().stream()
						.filter(e -> e.getValue() instanceof Serializable)
						.collect(Collectors.toMap(Map.Entry::getKey, e -> (Serializable) e.getValue(), (o1, o2) -> o1, HashMap::new)));
	}

	private static boolean isField(final @NonNull Class<?> clazz, final @NonNull String name) {
		try {
			clazz.getDeclaredField(name);
			return true;
		} catch (final @SuppressWarnings("unused") NoSuchFieldException ex) {
			final Class<?> superclass = clazz.getSuperclass();
			return superclass != null && isField(superclass, name);
		} catch (final @SuppressWarnings("unused") SecurityException ex) {
			return false;
		}
	}
}
