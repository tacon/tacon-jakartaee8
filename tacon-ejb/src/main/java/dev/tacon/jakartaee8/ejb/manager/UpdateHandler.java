package dev.tacon.jakartaee8.ejb.manager;

import java.util.List;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException.ValidationError;
import dev.tacon.jakartaee8.ejb.util.ValidationUtils;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface UpdateHandler<E extends AbstractJpaEntity<?>, D>
		extends UpdateValidator<E, D>, BeforeUpdateHandler<E, D>, AfterUpdateHandler<E, D> {

	@SuppressWarnings("rawtypes")
	UpdateHandler NO_OP = new UpdateHandler() {};

	@Override
	default void validateUpdate(final @NonNull D dto) throws ManagerValidationException {
		final List<ValidationError> errors = ValidationUtils.validate(dto);
		if (!errors.isEmpty()) {
			throw new ManagerValidationException(errors);
		}
	}

	@Override
	default void beforeUpdate(final @NonNull D dto, final @NonNull E entity) throws ManagerException {}

	@Override
	default void afterUpdate(final @NonNull D dto, final @NonNull E entity) throws ManagerException {}
}
