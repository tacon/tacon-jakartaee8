package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface BeforeUpdateHandler<E extends AbstractJpaEntity<?>, D> {

	/**
	 * @throws ManagerException
	 */
	default void updateEntityNotFound() throws ManagerException {}

	void beforeUpdate(@NonNull D dto, @NonNull E entity) throws ManagerException;

}
