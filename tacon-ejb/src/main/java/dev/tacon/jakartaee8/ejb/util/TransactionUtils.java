package dev.tacon.jakartaee8.ejb.util;

import javax.transaction.UserTransaction;

import dev.tacon.jakartaee8.ejb.eao.ApplicationEAO;

public final class TransactionUtils {

	private TransactionUtils() {
		throw new UnsupportedOperationException();
	}

	@FunctionalInterface
	public interface TransactionOperation {

		void run() throws Exception;
	}

	public interface TransactionResultSupplier<T> {

		T get() throws Exception;
	}

	public static void runInTransaction(final ApplicationEAO eao, final TransactionOperation operation) throws Exception {
		runInTransaction(eao.getSessionContext().getUserTransaction(), operation);
	}

	public static <T> T doInTransaction(final ApplicationEAO eao, final TransactionResultSupplier<T> operation) throws Exception {
		return doInTransaction(eao.getSessionContext().getUserTransaction(), operation);
	}

	public static <T> T doInTransaction(final UserTransaction transaction, final TransactionResultSupplier<T> operation) throws Exception {
		transaction.begin();
		try {
			final T result = operation.get();
			transaction.commit();
			return result;
		} catch (final Exception e) {
			try {
				transaction.rollback();
			} catch (final Exception er) {
				e.addSuppressed(er);
			}
			throw e instanceof RuntimeException ? new Exception("Exception in transaction", e) : e;
		}
	}

	public static void runInTransaction(final UserTransaction transaction, final TransactionOperation operation) throws Exception {
		transaction.begin();
		try {
			operation.run();
			transaction.commit();
		} catch (final Exception e) {
			try {
				transaction.rollback();
			} catch (final Exception er) {
				e.addSuppressed(er);
			}
			throw e instanceof RuntimeException ? new Exception("Exception in transaction", e) : e;
		}
	}
}