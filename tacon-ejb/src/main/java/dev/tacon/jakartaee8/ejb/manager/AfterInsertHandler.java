package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface AfterInsertHandler<E extends AbstractJpaEntity<?>, D> {

	void afterInsert(@NonNull D dto, @NonNull E entity) throws ManagerException;

}
