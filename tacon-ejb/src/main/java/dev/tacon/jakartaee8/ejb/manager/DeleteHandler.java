package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public interface DeleteHandler<E extends AbstractJpaEntity<?>>
		extends BeforeDeleteHandler<E>, AfterDeleteHandler<E> {

	@SuppressWarnings("rawtypes")
	DeleteHandler NO_OP = new DeleteHandler() {};

	@Override
	default void beforeDelete(final @NonNull E entity) throws ManagerException {}

	@Override
	default void afterDelete(final @NonNull E entity) throws ManagerException {}
}
