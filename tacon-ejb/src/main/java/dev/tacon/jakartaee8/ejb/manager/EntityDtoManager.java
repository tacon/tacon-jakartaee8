package dev.tacon.jakartaee8.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.dto.DtoWithID;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.dto.reference.DtoRef;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public abstract class EntityDtoManager<E extends AbstractJpaEntity<I>, I extends Serializable, D extends DtoWithID<?, K, R>, K extends Serializable, R extends DtoRef<? super D, K>, P extends Serializable>
		extends EntityBeanManager<E, I, D, P> {

	public EntityDtoManager() {}

	public void delete(final @NonNull R ref) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(ref, handler, handler);
	}

	public E delete(final @NonNull R ref, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		return super.delete(this.toId(ref), beforeDeleteHandler, afterDeleteHandler);
	}

	public @NonNull Optional<E> findEntity(final @Nullable D dto) {
		return dto != null ? this.findEntity(dto.getRef()) : Optional.empty();
	}

	public @NonNull Optional<E> findEntity(final @Nullable R ref) {
		return ref != null ? this.findEntity(this.toId(ref)) : Optional.empty();
	}

	public D findDto(final @Nullable R ref) {
		if (ref == null) {
			return null;
		}
		return this.findEntity(ref).map(this::toDto).orElse(null);
	}

	public E getEntityReference(final @Nullable R ref) {
		return ref != null ? this.getEntityReference(this.toId(ref)) : null;
	}

	protected abstract @NonNull I toId(final R ref);

	@Override
	protected final @NonNull I toId(final @NonNull D dto) {
		return this.toId(dto.getRef());
	}

	@Override
	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		R ref = dto.getRef();
		if (ref == null) {
			ref = this.newDtoRef(dto);
		}
		entity.setId(this.toId(ref));
	}

	/**
	 * @param dto
	 * @return
	 */
	protected R newDtoRef(final D dto) {
		return null;
	}

	public R toDtoRef(final @Nullable E entity) {
		return entity != null ? this.toDtoRef(entity.getId()) : null;
	}

	protected abstract @NonNull R toDtoRef(@NonNull I id);

}
