package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;

public interface InsertValidator<D> {

	void validateInsert(@NonNull D dto) throws ManagerValidationException;

}
