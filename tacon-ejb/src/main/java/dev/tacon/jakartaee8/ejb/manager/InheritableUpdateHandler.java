package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.dto.exception.ManagerValidationException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public class InheritableUpdateHandler<E extends AbstractJpaEntity<?>, D> implements UpdateHandler<E, D> {

	private final UpdateHandler<E, D> handler;

	public InheritableUpdateHandler(final @NonNull UpdateHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateUpdate(final @NonNull D dto) throws ManagerValidationException {
		this.handler.validateUpdate(dto);
	}

	@Override
	public void beforeUpdate(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		this.handler.beforeUpdate(dto, entity);
	}

	@Override
	public void afterUpdate(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		this.handler.afterUpdate(dto, entity);
	}

	@Override
	public void validateAganistEntity(final @NonNull D dto, final @NonNull E entity) throws ManagerValidationException {
		this.handler.validateAganistEntity(dto, entity);
	}

}
