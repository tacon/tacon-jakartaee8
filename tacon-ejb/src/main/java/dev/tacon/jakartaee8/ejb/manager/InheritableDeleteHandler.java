package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.exception.ManagerException;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public class InheritableDeleteHandler<E extends AbstractJpaEntity<?>> implements DeleteHandler<E> {

	private final DeleteHandler<E> handler;

	public InheritableDeleteHandler(final @NonNull DeleteHandler<E> handler) {
		this.handler = handler;
	}

	@Override
	public void beforeDelete(final @NonNull E entity) throws ManagerException {
		this.handler.beforeDelete(entity);
	}

	@Override
	public void afterDelete(final @NonNull E entity) throws ManagerException {
		this.handler.afterDelete(entity);
	}
}
