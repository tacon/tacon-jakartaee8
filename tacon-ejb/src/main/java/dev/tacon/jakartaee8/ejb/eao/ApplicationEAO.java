package dev.tacon.jakartaee8.ejb.eao;

import java.io.Serializable;

import javax.ejb.SessionContext;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee8.persistence.query.ISingleQuery;
import dev.tacon.jakartaee8.persistence.query.Queries;

public abstract class ApplicationEAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationEAO.class);

	public abstract EntityManager getEntityManager();

	public abstract SessionContext getSessionContext();

	public ISingleQuery<Void, Void> query() {
		return Queries.create(this.getEntityManager());
	}

	public <T> ISingleQuery<T, T> query(final @NonNull Class<T> entityClass) {
		return Queries.from(this.getEntityManager(), entityClass);
	}

	public <T extends AbstractJpaEntity<K>, K extends Serializable> T find(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.debug("find({}, {})", entityClass, id);
		return id != null ? this.getEntityManager().find(entityClass, id) : null;
	}

	public <T extends AbstractJpaEntity<K>, K extends Serializable> T getReference(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.debug("getReference({}, {})", entityClass, id);
		return id != null ? this.getEntityManager().getReference(entityClass, id) : null;
	}

	public <T> void insert(final T entity) {
		LOGGER.debug("insert {}", entity);
		this.getEntityManager().persist(entity);
	}

	public <T> T update(final T entity) {
		LOGGER.debug("update {}", entity);
		return this.getEntityManager().merge(entity);
	}

	public <T> void delete(final T entity) {
		LOGGER.debug("delete {}", entity);
		this.getEntityManager().remove(this.getEntityManager().merge(entity));
	}
}
