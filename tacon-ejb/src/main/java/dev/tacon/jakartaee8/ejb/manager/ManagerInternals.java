package dev.tacon.jakartaee8.ejb.manager;

import dev.tacon.jakartaee8.dto.DtoWithID;
import dev.tacon.jakartaee8.dto.exception.ManagerConcurrencyException;
import dev.tacon.jakartaee8.dto.ext.HasVersion;

@SuppressWarnings({ "rawtypes", "unchecked" })
class ManagerInternals {

	static void fillDtoVersion(final Object dto, final Object entity) {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable entityVersion = ((HasVersion) entity).getVersion();
			try {
				((HasVersion) dto).setVersion(entityVersion);
			} catch (final ClassCastException ex) {
				throw new IncompatibileVersionTypeException(entityVersion, ex);
			}
		}
	}

	static void checkVersion(final Object dto, final Object entity) throws ManagerConcurrencyException {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable dtoVersion = ((HasVersion) dto).getVersion();
			final Comparable entityVersion = ((HasVersion) entity).getVersion();
			if (dtoVersion != null && entityVersion != null) {
				final int compareResult;
				try {
					compareResult = dtoVersion.compareTo(entityVersion);
				} catch (final ClassCastException ex) {
					throw new IncompatibileVersionTypeException(dtoVersion, entityVersion, ex);
				}
				if (compareResult != 0) {
					String s = dto.getClass().getName();
					if (dto instanceof DtoWithID) {
						s += " (id=" + ((DtoWithID) dto).getId() + ")";
					}
					throw new ManagerConcurrencyException(s + " version (" + dtoVersion + ") != " + entity.getClass() + " version (" + entityVersion + ")");
				}
			}
		}
	}

	private ManagerInternals() {}

}
