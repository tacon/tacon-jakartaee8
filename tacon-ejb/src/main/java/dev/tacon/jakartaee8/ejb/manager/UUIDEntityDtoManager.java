package dev.tacon.jakartaee8.ejb.manager;

import java.io.Serializable;
import java.util.UUID;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.DtoWithUUID;
import dev.tacon.jakartaee8.dto.reference.DtoUUIDRef;
import dev.tacon.jakartaee8.persistence.entity.AbstractJpaEntity;

public abstract class UUIDEntityDtoManager<E extends AbstractJpaEntity<UUID>, D extends DtoWithUUID<D>, P extends Serializable>
		extends EntityDtoManager<E, UUID, D, UUID, DtoUUIDRef<D>, P> {

	@Override
	protected @NonNull DtoUUIDRef<D> newDtoRef(final D dto) {
		return new DtoUUIDRef<>(UUID.randomUUID());
	}

	@Override
	protected @NonNull UUID toId(final DtoUUIDRef<D> ref) {
		return ref.getId();
	}

	@Override
	protected @NonNull DtoUUIDRef<D> toDtoRef(final @NonNull UUID id) {
		return new DtoUUIDRef<>(id);
	}
}
