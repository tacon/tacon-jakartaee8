package dev.tacon.jakartaee8.ejb.manager;

public interface EntityFiller<K, D, E> {

	void fill(K key, D dto, E entity);

}
