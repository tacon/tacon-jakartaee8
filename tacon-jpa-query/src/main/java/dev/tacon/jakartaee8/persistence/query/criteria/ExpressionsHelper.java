package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.function.BiFunction;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;
import dev.tacon.misc.util.ClassUtils;

class ExpressionsHelper {

	@SuppressWarnings("unchecked")
	static Predicate isTrueUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr) {
		final Expression<?> expr = iExpr.resolve(data);
		final Class<?> type = ClassUtils.wrap(expr.getJavaType());
		if (Boolean.class == type) {
			return data.getCriteriaBuilder().isTrue((Expression<Boolean>) expr);
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"isTrue\" predicate");
	}

	@SuppressWarnings("unchecked")
	static Predicate isFalseUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr) {
		final Expression<?> expr = iExpr.resolve(data);
		final Class<?> type = ClassUtils.wrap(expr.getJavaType());
		if (Boolean.class == type) {
			return data.getCriteriaBuilder().isFalse((Expression<Boolean>) expr);
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"isFalse\" predicate");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate lessThanUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::lessThan, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate lessThanOrEqualToUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::lessThanOrEqualTo, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate greaterThanUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::greaterThan, iExpr, value);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static Predicate greaterThanOrEqualToUnchecked(final IQueryDataHolder data, final IExpression<?> iExpr, final Comparable value) {
		return compareUnchecked(data, data.getCriteriaBuilder()::greaterThanOrEqualTo, iExpr, value);
	}

	@SuppressWarnings("rawtypes")
	private static Predicate compareUnchecked(final IQueryDataHolder data, final BiFunction<Expression, Comparable, Predicate> predicate, final IExpression<?> iExpr, final Comparable value) {
		final Expression<?> expr = iExpr.resolve(data);
		final Class<?> type = ClassUtils.wrap(expr.getJavaType());
		if (Comparable.class.isAssignableFrom(type)) {
			return predicate.apply(expr, value);
		}
		throw new IllegalArgumentException("Invalid expression type " + type + " for \"lessThan\" predicate, must be comparable");
	}

	private ExpressionsHelper() {}
}
