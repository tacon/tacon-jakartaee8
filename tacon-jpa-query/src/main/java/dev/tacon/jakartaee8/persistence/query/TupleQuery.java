package dev.tacon.jakartaee8.persistence.query;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Selection;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.ITupleQuery.TupleResolver;
import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;
import dev.tacon.jakartaee8.persistence.query.criteria.ISelection;

class TupleQuery<F> extends Query<ITupleQuery<F>, F, Tuple> implements ITupleQuery<F>, TupleResolver {

	private final IdentityHashMap<ISelection<?>, Selection<?>> selections = new IdentityHashMap<>();

	TupleQuery(final EntityManager entityManager) {
		super(entityManager);
	}

	TupleQuery(final Query<?, F, ?> query, final ISelection<?>... selections) {
		super(query);
		for (final ISelection<?> selection : selections) {
			this.selections.put(selection, null);
		}
	}

	@Override
	protected Class<Tuple> getResultClass() {
		return Tuple.class;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<Tuple> criteriaQuery) {
		final List<Selection<?>> l = new ArrayList<>(this.selections.size());
		for (final Entry<ISelection<?>, Selection<?>> entry : this.selections.entrySet()) {
			final Selection<?> selection = entry.getKey().resolve(this);
			entry.setValue(selection);
			l.add(selection);
		}
		criteriaQuery.multiselect(l);
	}

	@Override
	protected void invalidateBeforeQuery() {
		// invalidation will be done manually
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ITupleQuery<T> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final TupleQuery<T> query = (TupleQuery<T>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	public <R> List<R> fetch(final TupleMapper<R> mapper) {
		return this.prepareQuery().getResultStream()
				.onClose(this::invalidate)
				.map(tuple -> mapper.map(tuple, this))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public void fetchAndThen(final TupleConsumer consumer) {
		this.prepareQuery().getResultList().forEach(t -> consumer.accept(t, this));
		this.invalidate();
	}

	@Override
	public <R> R fetchAndThenMap(final BiFunction<Stream<Tuple>, TupleResolver, R> mapper) {
		return mapper.apply(this.prepareQuery().getResultList().stream().onClose(this::invalidate), this);
	}

	@Override
	public <R> Optional<R> fetchFirst(final TupleMapper<R> mapper) {
		final TypedQuery<Tuple> query = this.prepareQuery();
		query.setMaxResults(1);
		final List<Tuple> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return Optional.empty();
		}
		final Optional<R> optional = Optional.ofNullable(resultList.get(0)).map(tuple -> mapper.map(tuple, this));
		this.invalidate();
		return optional;
	}

	@Override
	public <R> Stream<R> stream(final TupleMapper<R> mapper) {
		return this.prepareQuery().getResultStream()
				.map(tuple -> mapper.map(tuple, this))
				.onClose(this::invalidate);
	}

	@Override
	public <R> Optional<List<R>> fetchAsync(final SessionContext sessionContext, final TupleMapper<R> mapper) {
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		final TypedQuery<Tuple> query = this.prepareQuery();
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		final List<Tuple> resultList = query.getResultList();
		if (sessionContext.wasCancelCalled()) {
			this.invalidate();
			return Optional.empty();
		}
		return Optional.of(resultList.stream().map(tuple -> mapper.map(tuple, this))
				.onClose(this::invalidate)
				.collect(Collectors.toCollection(ArrayList::new)));
	}

	@Override
	public void fetchAsyncAndThen(final SessionContext sessionContext, final TupleConsumer consumer) {
		if (!sessionContext.wasCancelCalled()) {
			final TypedQuery<Tuple> query = this.prepareQuery();
			if (!sessionContext.wasCancelCalled()) {
				final List<Tuple> resultList = query.getResultList();
				if (!sessionContext.wasCancelCalled()) {
					resultList.forEach(t -> consumer.accept(t, this));
				}
			}
		}
		this.invalidate();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(final @NonNull Tuple tuple, final @NonNull ISelection<T> selection) {
		final Selection<T> sel = (Selection<T>) this.selections.get(selection);
		return tuple.get(sel != null ? sel : selection.resolve(this));
	}
}