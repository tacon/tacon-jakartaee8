package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SetAttribute;

import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

class PluralJoin<A extends PluralAttribute<? super S, C, T>, S, C, T> implements IJoin<S, T> {

	private final IFrom<?, S> parent;
	private final A attribute;
	private final JoinType joinType;
	private final String alias = "a" + UUID.randomUUID().toString().replace("-", "");

	PluralJoin(final IFrom<?, S> parent, final A attribute, final JoinType joinType) {
		this.parent = parent;
		this.attribute = attribute;
		this.joinType = joinType;
	}

	@Override
	public Join<S, T> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		return this.create(this.parent.resolve(data));
	}

	@Override
	@SuppressWarnings("unchecked")
	public Join<S, T> resolve(final IQueryDataHolder data) {
		Join<S, T> foundJoin = (Join<S, T>) data.getFrom(this);
		if (foundJoin != null) {
			return foundJoin;
		}

		final From<?, S> from = Objects.requireNonNull(
				this.parent.resolve(data),
				() -> "Cannot resolve parent join for " + this.attribute);

		for (final Join<S, ?> join : from.getJoins()) {
			if (join.getAlias().equals(this.alias)) {
				foundJoin = (Join<S, T>) join;
				data.registerJoin(this, foundJoin);
				return foundJoin;
			}
		}
		return this.create(from);
	}

	@SuppressWarnings("unchecked")
	private Join<S, T> create(final From<?, S> from) {
		final Join<S, T> join;
		switch (this.attribute.getCollectionType()) {
			case COLLECTION:
				join = from.join((CollectionAttribute<? super S, T>) this.attribute, this.joinType);
				break;
			case LIST:
				join = from.join((ListAttribute<? super S, T>) this.attribute, this.joinType);
				break;
			case MAP:
				join = from.join((MapAttribute<? super S, ?, T>) this.attribute, this.joinType);
				break;
			case SET:
				join = from.join((SetAttribute<? super S, T>) this.attribute, this.joinType);
				break;
			default:
				throw new IllegalArgumentException("Join has invalid collection type");
		}
		join.alias(this.alias);
		return join;
	}
}