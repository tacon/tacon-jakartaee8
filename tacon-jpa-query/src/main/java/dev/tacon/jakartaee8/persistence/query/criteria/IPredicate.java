package dev.tacon.jakartaee8.persistence.query.criteria;

import javax.persistence.criteria.Predicate;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface IPredicate extends IExpression<Boolean> {

	@Override
	Predicate resolve(IQueryDataHolder data);

	default IPredicate not() {
		return data -> this.resolve(data).not();
	}

	default IPredicate and(final @NonNull IPredicate predicate) {
		return data -> data.getCriteriaBuilder().and(this.resolve(data), predicate.resolve(data));
	}

	default IPredicate or(final @NonNull IPredicate predicate) {
		return data -> data.getCriteriaBuilder().or(this.resolve(data), predicate.resolve(data));
	}
}
