package dev.tacon.jakartaee8.persistence.query;

public final class Tuple3<T1, T2, T3> {

	public final T1 _1;
	public final T2 _2;
	public final T3 _3;

	public Tuple3(final T1 _1, final T2 _2, final T3 _3) {
		this._1 = _1;
		this._2 = _2;
		this._3 = _3;
	}
}
