package dev.tacon.jakartaee8.persistence.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ejb.SessionContext;

import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public interface ISingleQuery<F, R> extends IQuery<ISingleQuery<F, R>, F> {

	<T> ISingleQuery<T, R> from(IRoot<T> from, IFrom<?, ?>... froms);

	default <T> ISingleQuery<T, R> from(final Class<T> fromClass) {
		return this.from(Expressions.from(fromClass));
	}

	/**
	 * Executes the query and gets the result.
	 *
	 * @return the query result.
	 * @see jakarta.persistence.Query#getResultList()
	 */
	List<R> fetch();

	/**
	 * Executes the query and performs an operation
	 * for every element of the result.
	 *
	 * @param <E> possible exception during the
	 *            performed operation on the element.
	 * @throws E if any exception occurs during
	 *             the performed operations.
	 */
	<E extends Exception> void fetchAndThen(ResultCheckedConsumer<R, E> consumer) throws E;

	/**
	 * Executes the query returning limited to one result
	 * and returns the result as {@code Optional}.
	 *
	 * @return an empty {@code Optional} if there are
	 *         no results or the first result is {@code null},
	 *         otherwise an {@code Optional} containing
	 *         the first result value.
	 */
	Optional<R> fetchFirst();

	/**
	 * @see jakarta.persistence.Query#getSingleResult()
	 */
	R fetchSingleResult();

	/**
	 * Executes the query and gets a {@code Stream}
	 * over the result.
	 * <p>
	 * This is equivalent to:
	 *
	 * <pre>
	 * fetch().stream()
	 * </pre>
	 *
	 * </p>
	 *
	 * @return the query result as {@code Stream}.
	 * @see jakarta.persistence.Query#getResultList()
	 */
	Stream<R> stream();

	/**
	 * Executes the query and gets the result
	 * asynchronously.
	 * <br>
	 * If the execution is cancelled an empty
	 * {@code Optional} is returned.
	 *
	 * @return an {@code Optional} containing
	 *         the result or an empty {@code Optional}
	 *         if the execution was cancelled.
	 * @see jakarta.persistence.Query#getResultList()
	 * @see SessionContext#wasCancelCalled()
	 */
	Optional<List<R>> fetchAsync(SessionContext sessionContext);

	/**
	 * Executes the query asynchronously and performs
	 * an operation for every element of the result.
	 * <br>
	 * If the query execution is cancelled no
	 * operations are performed.
	 *
	 * @param <E> possibile exception during the
	 *            performed operation on the element.
	 * @throws E if any exception occurs during
	 *             the performed operations.
	 */
	<E extends Exception> void fetchAsyncAndThen(SessionContext sessionContext, ResultCheckedConsumer<R, E> consumer) throws E;

}
