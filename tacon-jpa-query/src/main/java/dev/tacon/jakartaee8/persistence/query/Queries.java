package dev.tacon.jakartaee8.persistence.query;

import static java.util.Objects.requireNonNull;

import javax.persistence.EntityManager;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

//@formatter:off
/**
 * Provider for fluent {@code IQuery} builder
 * implementations.
 * <p>
 * Calls must be in a fluent style, usage
 * of objects in an intermediate state
 * can lead to unpredictable behiavors.
 * </p>
 *
 * Valid:
 * <pre>
 * List persons = Queries.create(entityManager)
 * 		.from(from(Person.class))
 * 		.fetch();
 * </pre>
 * Invalid:
 * <pre>
 * ISingleQuery query = Queries.create(entityManager);
 * ISingleQuery query2 = query.from(from(Person.class));
 * query.from(from(Company.class));
 *
 * // NO: this fetches a list of companies !!
 * List persons = query2.fetch();
 * </pre>
 */
// @formatter:on
public final class Queries {

	public static ISingleQuery<Void, Void> create(final @NonNull EntityManager entityManager) {
		requireNonNull(entityManager, "Entity manager cannot be null");
		return new SingleQuery<>(entityManager);
	}

	public static <F> ISingleQuery<F, F> from(final @NonNull EntityManager entityManager, final @NonNull Class<F> rootClass) {
		requireNonNull(entityManager, "Entity manager cannot be null");
		final IRoot<F> root = Expressions.from(requireNonNull(rootClass, "Root query class cannot be null"));
		return create(entityManager).from(root).select(rootClass, root);
	}

	private Queries() {}
}