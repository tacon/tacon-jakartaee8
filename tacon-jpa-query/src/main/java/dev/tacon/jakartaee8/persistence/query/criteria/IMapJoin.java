package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.Map;

import javax.persistence.criteria.MapJoin;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.Expressions;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

public interface IMapJoin<Z, K, V> extends IPluralJoin<Z, V> {

	@Override
	MapJoin<Z, K, V> resolve(IQueryDataHolder data);

	default IPath<K> key() {
		return data -> this.resolve(data).key();
	}

	default IPath<V> value() {
		return data -> this.resolve(data).value();
	}

	default IExpression<Map.Entry<K, V>> entry() {
		return data -> this.resolve(data).entry();
	}

	@Override
	default IMapJoin<Z, K, V> on(final @NonNull IExpression<Boolean> condition) {
		return data -> this.resolve(data).on(condition.resolve(data));
	}

	@Override
	default IMapJoin<Z, K, V> on(final @NonNull IPredicate... conditions) {
		return data -> this.resolve(data).on(Expressions.and(conditions).resolve(data));
	}
}
