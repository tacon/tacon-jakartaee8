package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

class SingleJoin<S, T> implements IJoin<S, T> {

	private final IFrom<?, S> parent;
	private final SingularAttribute<? super S, T> attribute;
	private final JoinType joinType;
	private final String alias = "a" + UUID.randomUUID().toString().replace("-", "");

	SingleJoin(final IFrom<?, S> parent, final SingularAttribute<? super S, T> attribute, final JoinType joinType) {
		this.parent = parent;
		this.attribute = attribute;
		this.joinType = joinType;
	}

	@Override
	public Join<S, T> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		return this.create(this.parent.resolve(data));
	}

	private Join<S, T> create(final From<?, S> from) {
		final Join<S, T> join = from.join(this.attribute, this.joinType);
		join.alias(this.alias);
		return join;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Join<S, T> resolve(final IQueryDataHolder data) {
		Join<S, T> foundJoin = (Join<S, T>) data.getFrom(this);
		if (foundJoin != null) {
			return foundJoin;
		}

		final From<?, S> from = Objects.requireNonNull(
				this.parent.resolve(data),
				() -> "Cannot resolve parent join for " + this.attribute);

		for (final Join<S, ?> join : from.getJoins()) {
			if (join.getAlias().equals(this.alias)) {
				foundJoin = (Join<S, T>) join;
				data.registerJoin(this, foundJoin);
				return foundJoin;
			}
		}
		return this.create(from);
	}
}