package dev.tacon.jakartaee8.persistence.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Subquery;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.criteria.IExpression;
import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IJoin;
import dev.tacon.jakartaee8.persistence.query.criteria.IOrder;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;
import dev.tacon.jakartaee8.persistence.query.criteria.ISelection;

@SuppressWarnings({ "unchecked", "hiding" })
abstract class Query<Q, F, R> implements IQuery<Q, F>, IQueryDataHolder {

	protected final EntityManager entityManager;
	protected final CriteriaBuilder criteriaBuilder;
	private AbstractQuery<?> currentQuery;

	private IdentityHashMap<IFrom<?, ?>, From<?, ?>> froms;
	private IdentityHashMap<ISubquery<?, ?>, Subquery<?>> subqueries;
	private List<IPredicate> predicates;
	private List<IOrder> orders;
	private List<IExpression<?>> groups;
	private List<IPredicate> havingPredicates;
	private boolean distinct;
	private LockModeType lockMode;
	private int limit = -1;
	private int offset = -1;
	private Map<String, Object> hints;

	private IRoot<F> mainRoot;

	private boolean invalidated;

	Query(final EntityManager entityManager) {
		this.entityManager = entityManager;
		this.criteriaBuilder = entityManager.getCriteriaBuilder();
		this.froms = new IdentityHashMap<>();
	}

	protected Query(final Query<?, F, ?> query) {
		this.entityManager = query.entityManager;
		this.criteriaBuilder = query.criteriaBuilder;
		this.froms = query.froms;
		this.subqueries = query.subqueries;
		this.predicates = query.predicates;
		this.orders = query.orders;
		this.groups = query.groups;
		this.havingPredicates = query.havingPredicates;
		this.distinct = query.distinct;
		this.lockMode = query.lockMode;
		this.limit = query.limit;
		this.offset = query.offset;
		this.hints = query.hints;
		this.mainRoot = query.mainRoot;
		query.invalidate();
	}

	protected void invalidate() {
		this.invalidated = true;
		this.froms = null;
		this.subqueries = null;
		this.predicates = null;
		this.orders = null;
		this.groups = null;
		this.havingPredicates = null;
		this.hints = null;
		this.mainRoot = null;
	}

	protected void invalidateBeforeQuery() {
		this.invalidate();
	}

	@Override
	public <T> ISingleQuery<F, T> select(final Class<T> resultClass, final ISelection<T> selection) {
		return new SingleQuery<>(this, resultClass, selection);
	}

	@Override
	public ITupleQuery<F> select(final ISelection<?>... selections) {
		return new TupleQuery<>(this, selections);
	}

	@Override
	public <T1, T2> ISingleQuery<F, Tuple2<T1, T2>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2) {
		final Class<Tuple2<T1, T2>> resultClass = uncheckedCast(Tuple2.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data)));
	}

	@Override
	public <T1, T2, T3> ISingleQuery<F, Tuple3<T1, T2, T3>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3) {
		final Class<Tuple3<T1, T2, T3>> resultClass = uncheckedCast(Tuple3.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4> ISingleQuery<F, Tuple4<T1, T2, T3, T4>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4) {
		final Class<Tuple4<T1, T2, T3, T4>> resultClass = uncheckedCast(Tuple4.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4, T5> ISingleQuery<F, Tuple5<T1, T2, T3, T4, T5>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4, final ISelection<T5> s5) {
		final Class<Tuple5<T1, T2, T3, T4, T5>> resultClass = uncheckedCast(Tuple5.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data),
						s5.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4, T5, T6> ISingleQuery<F, Tuple6<T1, T2, T3, T4, T5, T6>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4, final ISelection<T5> s5, final ISelection<T6> s6) {
		final Class<Tuple6<T1, T2, T3, T4, T5, T6>> resultClass = uncheckedCast(Tuple6.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data),
						s5.resolve(data),
						s6.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4, T5, T6, T7> ISingleQuery<F, Tuple7<T1, T2, T3, T4, T5, T6, T7>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4, final ISelection<T5> s5, final ISelection<T6> s6, final ISelection<T7> s7) {
		final Class<Tuple7<T1, T2, T3, T4, T5, T6, T7>> resultClass = uncheckedCast(Tuple7.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data),
						s5.resolve(data),
						s6.resolve(data),
						s7.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4, T5, T6, T7, T8> ISingleQuery<F, Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4, final ISelection<T5> s5, final ISelection<T6> s6, final ISelection<T7> s7, final ISelection<T8> s8) {
		final Class<Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>> resultClass = uncheckedCast(Tuple8.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data),
						s5.resolve(data),
						s6.resolve(data),
						s7.resolve(data),
						s8.resolve(data)));
	}

	@Override
	public <T1, T2, T3, T4, T5, T6, T7, T8, T9> ISingleQuery<F, Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>> selectTuple(final ISelection<T1> s1, final ISelection<T2> s2, final ISelection<T3> s3, final ISelection<T4> s4, final ISelection<T5> s5, final ISelection<T6> s6, final ISelection<T7> s7, final ISelection<T8> s8, final ISelection<T9> s9) {
		final Class<Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>> resultClass = uncheckedCast(Tuple9.class);
		return new SingleQuery<>(this, resultClass,
				data -> data.getCriteriaBuilder().construct(resultClass,
						s1.resolve(data),
						s2.resolve(data),
						s3.resolve(data),
						s4.resolve(data),
						s5.resolve(data),
						s6.resolve(data),
						s7.resolve(data),
						s8.resolve(data),
						s9.resolve(data)));
	}

	protected void prepareSubqueries() {
		if (this.subqueries != null) {
			for (final Entry<ISubquery<?, ?>, Subquery<?>> entry : this.subqueries.entrySet()) {
				entry.setValue(entry.getKey().create(this));
			}
		}
	}

	protected abstract void prepareSelect(CriteriaQuery<R> criteriaQuery);

	protected void prepareFrom(final CriteriaQuery<R> criteriaQuery) {
		for (final Entry<IFrom<?, ?>, From<?, ?>> entry : this.froms.entrySet()) {
			entry.setValue(entry.getKey().create(this, criteriaQuery));
		}
	}

	protected void prepareWhere(final CriteriaQuery<R> criteriaQuery) {
		if (this.predicates != null) {
			criteriaQuery.where(this.predicates.stream().map(p -> p.resolve(this)).toArray(Predicate[]::new));
		}
	}

	protected void prepareOrder(final CriteriaQuery<R> criteriaQuery) {
		if (this.orders != null) {
			criteriaQuery.orderBy(this.orders.stream().map(o -> o.resolve(this)).collect(Collectors.toList()));
		}
	}

	protected void prepareGroup(final CriteriaQuery<R> criteriaQuery) {
		if (this.groups != null) {
			criteriaQuery.groupBy(this.groups.stream().map(g -> g.resolve(this)).collect(Collectors.toList()));
		}
	}

	protected void prepareHaving(final CriteriaQuery<R> criteriaQuery) {
		if (this.havingPredicates != null) {
			criteriaQuery.having(this.havingPredicates.stream().map(p -> p.resolve(this)).toArray(Predicate[]::new));
		}
	}

	protected abstract Class<R> getResultClass();

	protected TypedQuery<R> prepareQuery() {
		if (this.invalidated) {
			throw new IllegalStateException("Methods select or fetch have been called, this query class instance should not be used");
		}
		final CriteriaQuery<R> criteriaQuery = this.criteriaBuilder.createQuery(this.getResultClass());
		criteriaQuery.distinct(this.distinct);
		try {
			// XXX non thread-safe, mi serve questa variabile nelle resolve
			this.currentQuery = criteriaQuery;
			this.prepareFrom(criteriaQuery);
			// this.prepareSubqueries();
			this.prepareSelect(criteriaQuery);
			this.prepareWhere(criteriaQuery);
			this.prepareOrder(criteriaQuery);
			this.prepareGroup(criteriaQuery);
			this.prepareHaving(criteriaQuery);
		} finally {
			this.currentQuery = null;
		}
		final TypedQuery<R> query = this.entityManager.createQuery(criteriaQuery);
		if (this.lockMode != null) {
			query.setLockMode(this.lockMode);
		}
		if (this.limit >= 0) {
			query.setMaxResults(this.limit);
		}
		if (this.offset >= 0) {
			query.setFirstResult(this.offset);
		}
		if (this.hints != null) {
			this.hints.forEach(query::setHint);
		}
		this.invalidateBeforeQuery();
		return query;
	}

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return this.criteriaBuilder;
	}

	@Override
	public AbstractQuery<?> getCurrentQuery() {
		return this.currentQuery;
	}

	@Override
	public Q distinct() {
		this.distinct = true;
		return (Q) this;
	}

	protected void fromInternal(final @NonNull IRoot<F> root, final IFrom<?, ?>... froms) {
		this.froms.clear();
		this.mainRoot = Objects.requireNonNull(root);
		this.froms.put(root, null);
		for (final IFrom<?, ?> from : froms) {
			this.froms.put(from, null);
		}
	}

	@Override
	public <S, T> From<S, T> getFrom(final IFrom<S, T> from) {
		return (From<S, T>) this.froms.get(from);
	}

	@Override
	public <R> Subquery<R> getSubquery(final ISubquery<?, R> subquery, final Supplier<Subquery<R>> supplier) {
		if (this.subqueries == null) {
			this.subqueries = new IdentityHashMap<>();
		}
		return (Subquery<R>) this.subqueries.computeIfAbsent(subquery, k -> supplier.get());
	}

	@Override
	public <S, T> void registerJoin(final IJoin<S, T> iJoin, final Join<S, T> join) {
		this.froms.put(iJoin, join);
	}

	@Override
	public IRoot<?> getMainRoot() {
		return this.mainRoot;
	}

	@Override
	public Q where(final IPredicate... predicates) {
		Collections.addAll(this.getPredicates(), predicates);
		return (Q) this;
	}

	@Override
	public Q orderBy(final IOrder... orders) {
		Collections.addAll(this.getOrders(), orders);
		return (Q) this;
	}

	@Override
	public Q groupBy(final IExpression<?>... expressions) {
		if (this.groups == null) {
			this.groups = new ArrayList<>();
		}
		Collections.addAll(this.groups, expressions);
		return (Q) this;
	}

	@Override
	public Q having(final IPredicate... predicates) {
		if (this.havingPredicates == null) {
			this.havingPredicates = new ArrayList<>();
		}
		Collections.addAll(this.havingPredicates, predicates);
		return (Q) this;
	}

	@Override
	public Q lockMode(final LockModeType lockMode) {
		this.lockMode = lockMode;
		return (Q) this;
	}

	@Override
	public Q limit(final int limit) {
		this.limit = limit;
		return (Q) this;
	}

	@Override
	public Q offset(final int offset) {
		this.offset = offset;
		return (Q) this;
	}

	@Override
	public Q hint(final String key, final Object value) {
		if (this.hints == null) {
			this.hints = new HashMap<>();
		}
		this.hints.put(key, value);
		return (Q) this;
	}

	@Override
	public <V> Q eq(final IExpression<V> expr, final V value) {
		this.getPredicates().add(Expressions.equalOrNull(expr, value));
		return (Q) this;
	}

	@Override
	public <V> Q in(final IExpression<V> expression, final Collection<? extends V> values) {
		if (values == null) {
			this.getPredicates().add(Expressions.isNull(expression));
		} else if (values.isEmpty()) {
			this.getPredicates().add(Expressions.disjunction());
		} else {
			this.getPredicates().add(expression.in(values));
		}
		return (Q) this;
	}

	@Override
	public Q order(final boolean desc, final IExpression<?> expression) {
		this.getOrders().add(desc ? Expressions.desc(expression) : Expressions.asc(expression));
		return (Q) this;
	}

	private List<IPredicate> getPredicates() {
		if (this.predicates == null) {
			this.predicates = new ArrayList<>();
		}
		return this.predicates;
	}

	private List<IOrder> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<>();
		}
		return this.orders;
	}

	private static <T> Class<T> uncheckedCast(final Class<?> c) {
		return (Class<T>) c;
	}
}