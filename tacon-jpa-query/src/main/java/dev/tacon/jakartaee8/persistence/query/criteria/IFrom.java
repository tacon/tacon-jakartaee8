package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.function.Function;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface IFrom<Z, X> extends IPath<X> {

	From<Z, X> create(final IQueryDataHolder data, AbstractQuery<?> criteriaQuery);

	@Override
	default From<Z, X> resolve(final IQueryDataHolder data) {
		return data.getFrom(this);
	}

	default <Y> IJoin<X, Y> join(final @NonNull SingularAttribute<? super X, Y> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <Y> IJoin<X, Y> join(final @NonNull SingularAttribute<? super X, Y> attribute, final @NonNull Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return this.join(attribute, JoinType.INNER).onCondition(onCondition);
	}

	default <Y> IJoin<X, Y> join(final @NonNull SingularAttribute<? super X, Y> attribute, final @NonNull JoinType type) {
		return new SingleJoin<>(this, attribute, type);
	}

	default <Y> IJoin<X, Y> join(final @NonNull SingularAttribute<? super X, Y> attribute, final @NonNull JoinType type, final @NonNull Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return new SingleJoin<>(this, attribute, type).onCondition(onCondition);
	}

	default <C, Y> IJoin<X, Y> join(final @NonNull PluralAttribute<? super X, C, Y> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <C, Y> IJoin<X, Y> join(final @NonNull PluralAttribute<? super X, C, Y> attribute, final @NonNull Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return this.join(attribute, JoinType.INNER).onCondition(onCondition);
	}

	default <C, Y> IJoin<X, Y> join(final @NonNull PluralAttribute<? super X, C, Y> attribute, final @NonNull JoinType type) {
		return new PluralJoin<>(this, attribute, type);
	}

	default <C, Y> IJoin<X, Y> join(final @NonNull PluralAttribute<? super X, C, Y> attribute, final @NonNull JoinType type, final @NonNull Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return new PluralJoin<>(this, attribute, type).onCondition(onCondition);
	}

	default <K, V> IMapJoin<X, K, V> join(final @NonNull MapAttribute<? super X, K, V> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <K, V> IMapJoin<X, K, V> join(final @NonNull MapAttribute<? super X, K, V> attribute, final @NonNull JoinType type) {
		return new PluralMapJoin<>(this, attribute, type);
	}
}
