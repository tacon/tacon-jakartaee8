package dev.tacon.jakartaee8.persistence.query;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.persistence.criteria.CriteriaBuilder.Trimspec;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;
import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.persistence.query.criteria.IExpression;
import dev.tacon.jakartaee8.persistence.query.criteria.IOrder;
import dev.tacon.jakartaee8.persistence.query.criteria.IPath;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public final class Expressions {

	public static @NonNull <S> IRoot<S> from(final @NonNull Class<S> entityClass) {
		return (data, cq) -> cq.from(entityClass);
	}

	public static @NonNull ISubquery<Void, Void> subquery() {
		return new dev.tacon.jakartaee8.persistence.query.Subquery<>(Void.class);
	}

	/*
	 * BASIC PREDICATES
	 */

	public static @NonNull IPredicate opt(final @NonNull Optional<IPredicate> predicate) {
		return data -> predicate.isPresent()
				? predicate.get().resolve(data)
				: data.getCriteriaBuilder().conjunction();
	}

	public static @NonNull IPredicate opt(final @NonNull Supplier<IPredicate> predicate) {
		return data -> Optional.ofNullable(predicate.get())
				.map(p -> p.resolve(data))
				.orElse(data.getCriteriaBuilder().conjunction());
	}

	public static @NonNull IPredicate opt(final @NonNull IPredicate predicate, final @Nullable Object valueToBeNotNull) {
		return data -> valueToBeNotNull != null
				? predicate.resolve(data)
				: data.getCriteriaBuilder().conjunction();
	}

	public static @NonNull <T> IPredicate opt(final @Nullable T valueToBeNotNull, final Function<T, IPredicate> predicate) {
		return data -> valueToBeNotNull != null
				? predicate.apply(valueToBeNotNull).resolve(data)
				: data.getCriteriaBuilder().conjunction();
	}

	public static @NonNull IPredicate not(final @NonNull IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().not(expr.resolve(data));
	}

	public static @NonNull IPredicate and(final @NonNull IPredicate... predicates) {
		return data -> data.getCriteriaBuilder().and(resolve(data, predicates));
	}

	public static @NonNull IPredicate or(final @NonNull IPredicate... predicates) {
		return data -> data.getCriteriaBuilder().or(resolve(data, predicates));
	}

	public static @NonNull IPredicate isNull(final @NonNull IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().isNull(expr.resolve(data));
	}

	public static @NonNull IPredicate isNotNull(final @NonNull IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().isNotNull(expr.resolve(data));
	}

	public static @NonNull IPredicate disjunction() {
		return data -> data.getCriteriaBuilder().disjunction();
	}

	public static @NonNull IPredicate conjunction() {
		return data -> data.getCriteriaBuilder().conjunction();
	}

	/*
	 * EQUALITY PREDICATES
	 */

	public static @NonNull IPredicate isTrue(final @NonNull IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().isTrue(expr.resolve(data));
	}

	public static @NonNull IPredicate isFalse(final @NonNull IExpression<Boolean> expr) {
		return data -> data.getCriteriaBuilder().isFalse(expr.resolve(data));
	}

	public static @NonNull <T> IPredicate equal(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().equal(expr.resolve(data), value);
	}

	public static @NonNull <T> IPredicate equal(final @NonNull IExpression<? extends T> expr1, final @NonNull IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().equal(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T> IPredicate notEqual(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().notEqual(expr.resolve(data), value);
	}

	public static @NonNull <T> IPredicate notEqual(final @NonNull IExpression<? extends T> expr1, final @NonNull IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().notEqual(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T> IPredicate equalOrNull(final @NonNull IExpression<T> expr, final T value) {
		return value != null ? equal(expr, value) : isNull(expr);
	}

	public static @NonNull <T> IPredicate notEqualNorNull(final @NonNull IExpression<T> expr, final T value) {
		return value != null ? notEqual(expr, value) : isNotNull(expr);
	}

	/*
	 * STRING PREDICATES
	 */

	public static @NonNull IPredicate like(final @NonNull IExpression<String> expr, final String value) {
		return data -> data.getCriteriaBuilder().like(expr.resolve(data), value);
	}

	public static @NonNull IPredicate like(final @NonNull IExpression<String> expr1, final @NonNull IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().like(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull IPredicate notLike(final @NonNull IExpression<String> expr, final String value) {
		return data -> data.getCriteriaBuilder().notLike(expr.resolve(data), value);
	}

	public static @NonNull IPredicate notLke(final @NonNull IExpression<String> expr1, final @NonNull IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().notLike(expr1.resolve(data), expr2.resolve(data));
	}

	/*
	 * COMPARISON PREDICATES
	 */

	public static @NonNull <T extends Comparable<? super T>> IPredicate ge(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().greaterThanOrEqualTo(expr.resolve(data), value);
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate ge(final @NonNull IExpression<T> expr1, final @NonNull IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().greaterThanOrEqualTo(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate gt(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().greaterThan(expr.resolve(data), value);
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate gt(final @NonNull IExpression<T> expr1, final @NonNull IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().greaterThan(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate le(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().lessThanOrEqualTo(expr.resolve(data), value);
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate le(final @NonNull IExpression<T> expr1, final @NonNull IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().lessThanOrEqualTo(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate lt(final @NonNull IExpression<T> expr, final T value) {
		return data -> data.getCriteriaBuilder().lessThan(expr.resolve(data), value);
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate lt(final @NonNull IExpression<T> expr1, final @NonNull IExpression<T> expr2) {
		return data -> data.getCriteriaBuilder().lessThan(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate between(final @NonNull IExpression<T> expr, final T upperBound, final T lowerBound) {
		return data -> data.getCriteriaBuilder().between(expr.resolve(data), upperBound, lowerBound);
	}

	public static @NonNull <T extends Comparable<? super T>> IPredicate between(final @NonNull IExpression<T> expr, final @NonNull IExpression<T> upperExpr, final @NonNull IExpression<T> lowerExpr) {
		return data -> data.getCriteriaBuilder().between(expr.resolve(data), upperExpr.resolve(data), lowerExpr.resolve(data));
	}

	/*
	 * COLLECTION PREDICATES
	 */

	public static @NonNull <C extends Collection<?>> IPredicate isEmpty(final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isEmpty(collection.resolve(data));
	}

	public static @NonNull <C extends Collection<?>> IPredicate isNotEmpty(final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotEmpty(collection.resolve(data));
	}

	public static @NonNull <E, C extends Collection<E>> IPredicate isMember(final E elem, final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isMember(elem, collection.resolve(data));
	}

	public static @NonNull <E, C extends Collection<E>> IPredicate isMember(final @NonNull IExpression<E> elem, final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isMember(elem.resolve(data), collection.resolve(data));
	}

	public static @NonNull <E, C extends Collection<E>> IPredicate isNotMember(final E elem, final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotMember(elem, collection.resolve(data));
	}

	public static @NonNull <E, C extends Collection<E>> IPredicate isNotMember(final @NonNull IExpression<E> elem, final @NonNull IExpression<C> collection) {
		return data -> data.getCriteriaBuilder().isNotMember(elem.resolve(data), collection.resolve(data));
	}

	/*
	 * SUBQUERY PREDICATES
	 */

	public static @NonNull IPredicate exists(final ISubquery<?, ?> subquery) {
		return data -> data.getCriteriaBuilder().exists(subquery.resolve(data));
	}

	public static @NonNull <Y> IExpression<Y> all(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().all(subquery.resolve(data));
	}

	public static @NonNull <Y> IExpression<Y> some(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().some(subquery.resolve(data));
	}

	public static @NonNull <Y> IExpression<Y> any(final ISubquery<?, Y> subquery) {
		return data -> data.getCriteriaBuilder().any(subquery.resolve(data));
	}

	/*
	 * UTILITY FUNCTIONS
	 */

	public static @NonNull <X> IPath<X> rootPath() {
		return data -> Expressions.<X> resolveRoot(data);
	}

	public static @NonNull <X, T> IPath<T> path(final @NonNull SingularAttribute<? super X, T> attribute) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute);
	}

	public static @NonNull <X, Y, T> IPath<T> path(final @NonNull SingularAttribute<? super X, Y> attribute1, final @NonNull SingularAttribute<? super Y, T> attribute2) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute1).get(attribute2);
	}

	public static @NonNull <X, Y, Z, T> IPath<T> path(final @NonNull SingularAttribute<? super X, Y> attribute1, final @NonNull SingularAttribute<? super Y, Z> attribute2, final @NonNull SingularAttribute<? super Z, T> attribute3) {
		return data -> Expressions.<X> resolveRoot(data).get(attribute1).get(attribute2).get(attribute3);
	}

	public static @NonNull <T> IExpression<T> literal(final @NonNull T value) {
		return data -> data.getCriteriaBuilder().literal(value);
	}

	public static @NonNull <T> IExpression<T> nullLiteral(final @NonNull Class<T> valueClass) {
		return data -> data.getCriteriaBuilder().nullLiteral(valueClass);
	}

	public static @NonNull <T> IExpression<T> coalesce(final @NonNull IExpression<? extends T> expr, final T value) {
		return data -> data.getCriteriaBuilder().coalesce(expr.resolve(data), value);
	}

	public static @NonNull <T> IExpression<T> coalesce(final @NonNull IExpression<? extends T> expr1, final @NonNull IExpression<? extends T> expr2) {
		return data -> data.getCriteriaBuilder().coalesce(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <Y> IExpression<Y> nullif(final @NonNull IExpression<Y> x, final @NonNull IExpression<?> y) {
		return data -> data.getCriteriaBuilder().nullif(x.resolve(data), y.resolve(data));
	}

	public static @NonNull <Y> IExpression<Y> nullif(final @NonNull IExpression<Y> x, final Y y) {
		return data -> data.getCriteriaBuilder().nullif(x.resolve(data), y);
	}

	public static @NonNull <T> IExpression<T> iif(final @NonNull IExpression<Boolean> condition, final @NonNull IExpression<? extends T> then, final @NonNull IExpression<? extends T> otherwise) {
		return data -> data.getCriteriaBuilder().<T> selectCase().when(condition.resolve(data), then.resolve(data)).otherwise(otherwise.resolve(data));
	}

	public static @NonNull <T> IExpression<T> iif(final @NonNull IExpression<Boolean> condition, final T then, final T otherwise) {
		return data -> data.getCriteriaBuilder().<T> selectCase().when(condition.resolve(data), then).otherwise(otherwise);
	}

	public static @NonNull <T> IExpression<T> function(final @NonNull String name, final @NonNull Class<T> type, final @NonNull IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().function(name, type, expr.resolve(data));
	}

	public static @NonNull <T> IExpression<T> function(final @NonNull String name, final @NonNull Class<T> type, final @NonNull IExpression<?>... exprs) {
		return data -> data.getCriteriaBuilder().function(name, type, resolve(data, exprs));
	}

	public static @NonNull <T, D extends T> IPath<D> treat(final @NonNull IPath<T> resolver, final @NonNull Class<D> type) {
		return data -> data.getCriteriaBuilder().treat(resolver.resolve(data), type);
	}

	/*
	 * STRING FUNCTIONS
	 */

	public static @NonNull IExpression<Integer> length(final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().length(expr.resolve(data));
	}

	public static @NonNull IExpression<Integer> locate(final @NonNull IExpression<String> expr, final @NonNull IExpression<String> patternExpr) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), patternExpr.resolve(data));
	}

	public static @NonNull IExpression<Integer> locate(final @NonNull IExpression<String> expr, final String pattern) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), pattern);
	}

	public static @NonNull IExpression<Integer> locate(final @NonNull IExpression<String> expr, final @NonNull IExpression<String> patternExpr, final @NonNull IExpression<Integer> fromExpr) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), patternExpr.resolve(data), fromExpr.resolve(data));
	}

	public static @NonNull IExpression<Integer> locate(final @NonNull IExpression<String> expr, final String pattern, final int from) {
		return data -> data.getCriteriaBuilder().locate(expr.resolve(data), pattern, from);
	}

	public static @NonNull IExpression<String> concat(final @NonNull IExpression<String> expr1, final @NonNull IExpression<String> expr2) {
		return data -> data.getCriteriaBuilder().concat(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull IExpression<String> concat(final @NonNull IExpression<String> expr, final String string) {
		return data -> data.getCriteriaBuilder().concat(expr.resolve(data), string);
	}

	public static @NonNull IExpression<String> concat(final String string, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().concat(string, expr.resolve(data));
	}

	public static @NonNull IExpression<String> lower(final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().lower(expr.resolve(data));
	}

	public static @NonNull IExpression<String> upper(final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().upper(expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final Trimspec ts, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final @NonNull IExpression<Character> exprChar, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(exprChar.resolve(data), expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final Trimspec ts, final @NonNull IExpression<Character> exprChar, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, exprChar.resolve(data), expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final char c, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(c, expr.resolve(data));
	}

	public static @NonNull IExpression<String> trim(final Trimspec ts, final char c, final @NonNull IExpression<String> expr) {
		return data -> data.getCriteriaBuilder().trim(ts, c, expr.resolve(data));
	}

	/*
	 * NUMERIC FUNCTIONS
	 */

	public static @NonNull <N extends Number> IExpression<N> abs(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().abs(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> sum(final @NonNull IExpression<? extends N> expr1, final @NonNull IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().sum(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> sum(final @NonNull IExpression<? extends N> expr, final @NonNull N value) {
		return data -> data.getCriteriaBuilder().sum(expr.resolve(data), value);
	}

	public static @NonNull <N extends Number> IExpression<N> diff(final @NonNull IExpression<? extends N> expr1, final @NonNull IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().diff(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> diff(final @NonNull IExpression<? extends N> expr, final @NonNull N value) {
		return data -> data.getCriteriaBuilder().diff(expr.resolve(data), value);
	}

	public static @NonNull <N extends Number> IExpression<N> prod(final @NonNull IExpression<? extends N> expr1, final @NonNull IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().prod(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> prod(final @NonNull IExpression<? extends N> expr, final @NonNull N value) {
		return data -> data.getCriteriaBuilder().prod(expr.resolve(data), value);
	}

	public static @NonNull <N extends Number> IExpression<Number> quot(final @NonNull IExpression<? extends N> expr1, final @NonNull IExpression<? extends N> expr2) {
		return data -> data.getCriteriaBuilder().quot(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull IExpression<Number> quot(final @NonNull IExpression<? extends Number> expr, final @NonNull Number value) {
		return data -> data.getCriteriaBuilder().quot(expr.resolve(data), value);
	}

	public static @NonNull IExpression<Integer> mod(final @NonNull IExpression<Integer> expr1, final @NonNull IExpression<Integer> expr2) {
		return data -> data.getCriteriaBuilder().mod(expr1.resolve(data), expr2.resolve(data));
	}

	public static @NonNull IExpression<Integer> mod(final @NonNull IExpression<Integer> expr, final Integer value) {
		return data -> data.getCriteriaBuilder().mod(expr.resolve(data), value);
	}

	public static @NonNull IExpression<Integer> mod(final Integer value, final @NonNull IExpression<Integer> expr) {
		return data -> data.getCriteriaBuilder().mod(value, expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> neg(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().neg(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<Double> sqrt(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().sqrt(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<BigDecimal> toBigDecimal(final @NonNull IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toBigDecimal(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<Double> toDouble(final @NonNull IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toDouble(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<Long> toLong(final @NonNull IExpression<? extends N> expr) {
		return data -> data.getCriteriaBuilder().toLong(expr.resolve(data));
	}

	/*
	 * DATE FUNCTIONS
	 */

	public static @NonNull IExpression<java.sql.Date> currentDate() {
		return data -> data.getCriteriaBuilder().currentDate();
	}

	public static @NonNull IExpression<java.sql.Timestamp> currentTimestamp() {
		return data -> data.getCriteriaBuilder().currentTimestamp();
	}

	public static @NonNull IExpression<java.sql.Time> currentTime() {
		return data -> data.getCriteriaBuilder().currentTime();
	}

	/*
	 * ACCUMULATOR FUNCTIONS
	 */

	public static @NonNull IExpression<Long> count() {
		return data -> data.getCriteriaBuilder().count(data.getMainRoot().resolve(data));
	}

	public static @NonNull <Y> IExpression<Long> count(final @NonNull IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().count(expr.resolve(data));
	}

	public static @NonNull <Y> IExpression<Long> countDistinct(final @NonNull IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().countDistinct(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> sum(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().sum(expr.resolve(data));
	}

	public static @NonNull IExpression<Long> sumAsLong(final @NonNull IExpression<Integer> expr) {
		return data -> data.getCriteriaBuilder().sumAsLong(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<Double> avg(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().avg(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> min(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().min(expr.resolve(data));
	}

	public static @NonNull <N extends Number> IExpression<N> max(final @NonNull IExpression<N> expr) {
		return data -> data.getCriteriaBuilder().max(expr.resolve(data));
	}

	public static @NonNull <Y extends Comparable<? super Y>> IExpression<Y> least(final @NonNull IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().least(expr.resolve(data));
	}

	public static @NonNull <Y extends Comparable<? super Y>> IExpression<Y> greatest(final @NonNull IExpression<Y> expr) {
		return data -> data.getCriteriaBuilder().greatest(expr.resolve(data));
	}

	/*
	 * MAP FUNCTIONS
	 */

	public static @NonNull <K, V> IExpression<Set<K>> keys(final Map<K, V> map) {
		return data -> data.getCriteriaBuilder().keys(map);
	}

	public static @NonNull <K, V> IExpression<Collection<V>> values(final Map<K, V> map) {
		return data -> data.getCriteriaBuilder().values(map);
	}

	/*
	 * ORDERS
	 */

	public static @NonNull IOrder asc(final @NonNull IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().asc(expr.resolve(data));
	}

	public static @NonNull IOrder desc(final @NonNull IExpression<?> expr) {
		return data -> data.getCriteriaBuilder().desc(expr.resolve(data));
	}

	/*
	 * INTERNAL
	 */

	@SuppressWarnings("unchecked")
	private static <X> From<?, X> resolveRoot(final IQueryDataHolder data) {
		final IRoot<?> mainRoot = requireNonNull(data.getMainRoot(), "No root defined for this query");
		try {
			return (From<?, X>) mainRoot.resolve(data);
		} catch (final @SuppressWarnings("unused") ClassCastException ex) {
			throw new ClassCastException("Root type is not valid for this attribute");
		}
	}

	private static Predicate[] resolve(final IQueryDataHolder data, final IPredicate[] predicates) {
		if (predicates.length == 0) {
			return new Predicate[0];
		}
		final Predicate[] res = new Predicate[predicates.length];
		for (int i = 0; i < predicates.length; i++) {
			res[i] = predicates[i].resolve(data);
		}
		return res;
	}

	private static Expression<?>[] resolve(final IQueryDataHolder data, final IExpression<?>[] expressions) {
		if (expressions.length == 0) {
			return new Expression[0];
		}
		final Expression<?>[] res = new Expression[expressions.length];
		for (int i = 0; i < expressions.length; i++) {
			res[i] = expressions[i].resolve(data);
		}
		return res;
	}

	private Expressions() {}
}