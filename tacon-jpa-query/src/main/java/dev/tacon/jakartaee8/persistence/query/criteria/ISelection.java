package dev.tacon.jakartaee8.persistence.query.criteria;

import javax.persistence.criteria.Selection;

import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface ISelection<T> {

	Selection<T> resolve(IQueryDataHolder data);
}
