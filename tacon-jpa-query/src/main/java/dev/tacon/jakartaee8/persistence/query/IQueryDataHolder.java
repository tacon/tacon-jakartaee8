package dev.tacon.jakartaee8.persistence.query;

import java.util.function.Supplier;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Subquery;

import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IJoin;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public interface IQueryDataHolder {

	CriteriaBuilder getCriteriaBuilder();

	AbstractQuery<?> getCurrentQuery();

	<S, T> void registerJoin(IJoin<S, T> iJoin, Join<S, T> join);

	<S, T> From<S, T> getFrom(IFrom<S, T> from);

	<R> Subquery<R> getSubquery(ISubquery<?, R> subquery, Supplier<Subquery<R>> supplier);

	IRoot<?> getMainRoot();

}
