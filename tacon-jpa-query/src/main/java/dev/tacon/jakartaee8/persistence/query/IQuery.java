package dev.tacon.jakartaee8.persistence.query;

import static dev.tacon.jakartaee8.persistence.query.Expressions.path;

import java.util.Collection;

import javax.persistence.LockModeType;
import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.jakartaee8.persistence.query.criteria.IExpression;
import dev.tacon.jakartaee8.persistence.query.criteria.IOrder;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.ISelection;

public interface IQuery<Q, F> {

	Q distinct();

	<T> ISingleQuery<F, T> select(Class<T> resultClass, ISelection<T> selection);

	default <T> ISingleQuery<F, T> select(final SingularAttribute<? super F, T> attribute) {
		return this.select(attribute.getJavaType(), path(attribute));
	}

	ITupleQuery<F> select(ISelection<?>... selections);

	<T1, T2> ISingleQuery<F, Tuple2<T1, T2>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2);

	<T1, T2, T3> ISingleQuery<F, Tuple3<T1, T2, T3>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3);

	<T1, T2, T3, T4> ISingleQuery<F, Tuple4<T1, T2, T3, T4>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4);

	<T1, T2, T3, T4, T5> ISingleQuery<F, Tuple5<T1, T2, T3, T4, T5>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4, ISelection<T5> sel5);

	<T1, T2, T3, T4, T5, T6> ISingleQuery<F, Tuple6<T1, T2, T3, T4, T5, T6>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4, ISelection<T5> sel5, ISelection<T6> sel6);

	<T1, T2, T3, T4, T5, T6, T7> ISingleQuery<F, Tuple7<T1, T2, T3, T4, T5, T6, T7>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4, ISelection<T5> sel5, ISelection<T6> sel6, ISelection<T7> sel7);

	<T1, T2, T3, T4, T5, T6, T7, T8> ISingleQuery<F, Tuple8<T1, T2, T3, T4, T5, T6, T7, T8>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4, ISelection<T5> sel5, ISelection<T6> sel6, ISelection<T7> sel7, ISelection<T8> sel8);

	<T1, T2, T3, T4, T5, T6, T7, T8, T9> ISingleQuery<F, Tuple9<T1, T2, T3, T4, T5, T6, T7, T8, T9>> selectTuple(ISelection<T1> sel1, ISelection<T2> sel2, ISelection<T3> sel3, ISelection<T4> sel4, ISelection<T5> sel5, ISelection<T6> sel6, ISelection<T7> sel7, ISelection<T8> sel8, ISelection<T9> sel9);

	Q where(IPredicate... predicates);

	Q orderBy(IOrder... orders);

	Q groupBy(IExpression<?>... expressions);

	Q having(IPredicate... predicates);

	Q lockMode(LockModeType lockMode);

	Q limit(int limit);

	Q offset(int offset);

	Q hint(String key, Object value);

	/**
	 * Restricts the query results applying a
	 * predicate for testing the specified expression
	 * against specified value, if the value is not {@code null}.
	 * <p>If the value is {@code null} the predicate will
	 * test whether the expression is {@code null}.</p>
	 *
	 * @param <V> the type of the expression
	 * @param expression expression to be tested
	 */
	<V> Q eq(IExpression<V> expression, V value);

	/**
	 * Restricts the query results applying a
	 * predicate for testing if the specified expression
	 * is contained the specified collection of values,
	 * if the collection is not {@code null} nor empty.
	 * <p>If the collection is {@code null} the predicate will
	 * test whether the expression is {@code null}.</p>
	 * <p>If the collection is empty the predicate
	 * will be a disjunction.</p>
	 *
	 * @param <V> the type of the expression
	 * @param values collection of values to be tested against
	 */
	<V> Q in(IExpression<V> expression, Collection<? extends V> values);

	default <V> Q eq(final SingularAttribute<? super F, V> attribute, final V value) {
		return this.eq(path(attribute), value);
	}

	default <V> Q in(final SingularAttribute<? super F, V> attribute, final Collection<? extends V> values) {
		return this.in(path(attribute), values);
	}

	default Q eq(final SingularAttribute<? super F, Boolean> attribute, final boolean value) {
		return this.eq(attribute, Boolean.valueOf(value));
	}

	default Q eq(final SingularAttribute<? super F, Integer> attribute, final int value) {
		return this.eq(attribute, Integer.valueOf(value));
	}

	default Q eq(final SingularAttribute<? super F, Long> attribute, final long value) {
		return this.eq(attribute, Long.valueOf(value));
	}

	default <X, V> Q eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, V> attribute2, final V value) {
		return this.eq(path(attribute1, attribute2), value);
	}

	default <X, Y, V> Q eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, Y> attribute2, final SingularAttribute<? super Y, V> attribute3, final V value) {
		return this.eq(path(attribute1, attribute2, attribute3), value);
	}

	default Q order(final IExpression<?> expression) {
		return this.order(false, expression);
	}

	Q order(boolean desc, IExpression<?> expression);

	default Q order(final SingularAttribute<? super F, ?> attribute) {
		return this.order(false, attribute);
	}

	default Q order(final boolean desc, final SingularAttribute<? super F, ?> attribute) {
		return this.order(desc, path(attribute));
	}

	default <X, V> Q order(final boolean desc, final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, V> attribute2) {
		return this.order(desc, path(attribute1, attribute2));
	}

	default <X, Y, V> Q order(final boolean desc, final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, Y> attribute2, final SingularAttribute<? super Y, V> attribute3) {
		return this.order(desc, path(attribute1, attribute2, attribute3));
	}
}
