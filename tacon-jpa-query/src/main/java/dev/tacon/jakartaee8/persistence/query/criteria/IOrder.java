package dev.tacon.jakartaee8.persistence.query.criteria;

import javax.persistence.criteria.Order;

import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface IOrder {

	Order resolve(IQueryDataHolder data);

}
