package dev.tacon.jakartaee8.persistence.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ejb.SessionContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;
import dev.tacon.jakartaee8.persistence.query.criteria.ISelection;

class SingleQuery<F, R> extends Query<ISingleQuery<F, R>, F, R> implements ISingleQuery<F, R> {

	private Class<R> resultClass;
	private ISelection<R> selection;

	SingleQuery(final EntityManager entityManager) {
		super(entityManager);
	}

	protected SingleQuery(final Query<?, F, ?> query, final Class<R> resultClass, final ISelection<R> selection) {
		super(query);
		this.resultClass = resultClass;
		this.selection = selection;
	}

	@Override
	protected Class<R> getResultClass() {
		return this.resultClass;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<R> criteriaQuery) {
		if (this.selection != null) {
			criteriaQuery.select(this.selection.resolve(this));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ISingleQuery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final SingleQuery<T, R> query = (SingleQuery<T, R>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	public List<R> fetch() {
		return this.prepareQuery().getResultList();
	}

	@Override
	public Stream<R> stream() {
		return this.prepareQuery().getResultStream();
	}

	@Override
	public <E extends Exception> void fetchAndThen(final ResultCheckedConsumer<R, E> consumer) throws E {
		for (final R r : this.prepareQuery().getResultList()) {
			consumer.accept(r);
		}
	}

	@Override
	public Optional<R> fetchFirst() {
		final TypedQuery<R> query = this.prepareQuery();
		query.setMaxResults(1);
		final List<R> resultList = query.getResultList();
		return resultList.isEmpty()
				? Optional.empty()
				: Optional.ofNullable(resultList.get(0));
	}

	@Override
	public R fetchSingleResult() {
		return this.prepareQuery().getSingleResult();
	}

	@Override
	public @NonNull Optional<List<R>> fetchAsync(final SessionContext sessionContext) {
		if (sessionContext.wasCancelCalled()) {
			return Optional.empty();
		}
		final TypedQuery<R> query = this.prepareQuery();
		if (sessionContext.wasCancelCalled()) {
			return Optional.empty();
		}
		final List<R> resultList = query.getResultList();
		return sessionContext.wasCancelCalled()
				? Optional.empty()
				: Optional.of(resultList);
	}

	@Override
	public <E extends Exception> void fetchAsyncAndThen(final SessionContext sessionContext, final ResultCheckedConsumer<R, E> consumer) throws E {
		if (sessionContext.wasCancelCalled()) {
			return;
		}
		final TypedQuery<R> query = this.prepareQuery();
		if (sessionContext.wasCancelCalled()) {
			return;
		}
		final List<R> resultList = query.getResultList();
		if (!sessionContext.wasCancelCalled()) {
			for (final R r : resultList) {
				consumer.accept(r);
			}
		}
	}
}