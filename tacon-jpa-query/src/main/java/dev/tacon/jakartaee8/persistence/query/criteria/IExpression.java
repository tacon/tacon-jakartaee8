package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.criteria.Expression;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.Expressions;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;
import dev.tacon.jakartaee8.persistence.query.ISubquery;

@FunctionalInterface
public interface IExpression<T> extends ISelection<T> {

	@Override
	Expression<T> resolve(IQueryDataHolder data);

	default <X> IExpression<X> cast(final @NonNull Class<X> type) {
		return data -> this.resolve(data).as(type);
	}

	default IPredicate isNull() {
		return data -> this.resolve(data).isNull();
	}

	default IPredicate isNotNull() {
		return data -> this.resolve(data).isNotNull();
	}

	default IPredicate eq(final T value) {
		return Expressions.equalOrNull(this, value);
	}

	default IPredicate eq(final @NonNull IExpression<? extends T> expr) {
		return Expressions.equal(this, expr);
	}

	default IPredicate notEq(final T value) {
		return Expressions.notEqualNorNull(this, value);
	}

	default IPredicate notEq(final @NonNull IExpression<? extends T> expr) {
		return Expressions.notEqual(this, expr);
	}

	default IPredicate isTrue() {
		return data -> ExpressionsHelper.isTrueUnchecked(data, this);
	}

	default IPredicate isFalse() {
		return data -> ExpressionsHelper.isFalseUnchecked(data, this);
	}

	default <U extends Comparable<? super T>> IPredicate lt(final U value) {
		return data -> ExpressionsHelper.lessThanUnchecked(data, this, value);
	}

	default <U extends Comparable<? super T>> IPredicate le(final U value) {
		return data -> ExpressionsHelper.lessThanOrEqualToUnchecked(data, this, value);
	}

	default <U extends Comparable<? super T>> IPredicate gt(final U value) {
		return data -> ExpressionsHelper.greaterThanUnchecked(data, this, value);
	}

	default <U extends Comparable<? super T>> IPredicate ge(final U value) {
		return data -> ExpressionsHelper.greaterThanOrEqualToUnchecked(data, this, value);
	}

	default IExpression<T> orElse(final IExpression<? extends T> expr) {
		return Expressions.coalesce(this, expr);
	}

	default IExpression<T> orElse(final T value) {
		return Expressions.coalesce(this, value);
	}

	default IOrder asc() {
		return Expressions.asc(this);
	}

	default IOrder desc() {
		return Expressions.desc(this);
	}

	default <U extends T> IPredicate in(final @SuppressWarnings("unchecked") @NonNull U... values) {
		return data -> values.length == 0
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(values);
	}

	default IPredicate in(final @SuppressWarnings({ "unchecked" }) @NonNull IExpression<? extends T>... values) {
		return data -> values.length == 0
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(Arrays.stream(values).map(exp -> exp.resolve(data)).toArray(Expression[]::new));
	}

	default IPredicate in(final @NonNull Collection<? extends T> values) {
		return data -> values.isEmpty()
				? data.getCriteriaBuilder().disjunction()
				: this.resolve(data).in(values);
	}

	default IPredicate in(final @NonNull ISubquery<?, T> subquery) {
		return data -> this.resolve(data).in(subquery.resolve(data));
	}
}
