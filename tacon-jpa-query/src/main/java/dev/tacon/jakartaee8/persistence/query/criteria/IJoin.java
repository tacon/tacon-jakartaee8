package dev.tacon.jakartaee8.persistence.query.criteria;

import java.util.function.Function;

import javax.persistence.criteria.AbstractQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.Expressions;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface IJoin<Z, X> extends IFrom<Z, X> {

	@Override
	default From<Z, X> create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		throw new UnsupportedOperationException("Invalid join implementation");
	}

	@Override
	Join<Z, X> resolve(final IQueryDataHolder data);

	default IJoin<Z, X> on(final @NonNull IExpression<Boolean> condition) {
		return data -> this.resolve(data).on(condition.resolve(data));
	}

	default IJoin<Z, X> on(final @NonNull IPredicate... conditions) {
		return data -> this.resolve(data).on(Expressions.and(conditions).resolve(data));
	}

	default IJoin<Z, X> onCondition(final @NonNull Function<IJoin<Z, X>, IExpression<Boolean>> condition) {
		return data -> this.resolve(data).on(condition.apply(this).resolve(data));
	}
}
