package dev.tacon.jakartaee8.persistence.query;

@FunctionalInterface
public interface ResultCheckedConsumer<R, E extends Exception> {

	void accept(R result) throws E;
}
