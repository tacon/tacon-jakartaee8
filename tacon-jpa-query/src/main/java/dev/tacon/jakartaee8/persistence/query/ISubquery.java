package dev.tacon.jakartaee8.persistence.query;

import static dev.tacon.jakartaee8.persistence.query.Expressions.path;

import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.jakartaee8.persistence.query.criteria.IExpression;
import dev.tacon.jakartaee8.persistence.query.criteria.IFrom;
import dev.tacon.jakartaee8.persistence.query.criteria.IPredicate;
import dev.tacon.jakartaee8.persistence.query.criteria.IRoot;

public interface ISubquery<F, R> extends IExpression<R> {

	ISubquery<F, R> distinct();

	<T> ISubquery<F, T> select(Class<T> resultClass, IExpression<T> selection);

	default <T> ISubquery<F, T> select(final SingularAttribute<? super F, T> attribute) {
		return this.select(attribute.getJavaType(), path(attribute));
	}

	<T> ISubquery<T, R> from(IRoot<T> from, IFrom<?, ?>... froms);

	default <T> ISubquery<T, R> from(final Class<T> fromClass) {
		return this.from(Expressions.from(fromClass));
	}

	ISubquery<F, R> where(IPredicate... predicates);

	<V> ISubquery<F, R> eq(IExpression<V> expression, V value);

	default <V> ISubquery<F, R> eq(final SingularAttribute<? super F, V> attribute, final V value) {
		return this.eq(path(attribute), value);
	}

	default ISubquery<F, R> eq(final SingularAttribute<? super F, Boolean> attribute, final boolean value) {
		return this.eq(attribute, Boolean.valueOf(value));
	}

	default ISubquery<F, R> eq(final SingularAttribute<? super F, Integer> attribute, final int value) {
		return this.eq(attribute, Integer.valueOf(value));
	}

	default ISubquery<F, R> eq(final SingularAttribute<? super F, Long> attribute, final long value) {
		return this.eq(attribute, Long.valueOf(value));
	}

	default <X, V> ISubquery<F, R> eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, V> attribute2, final V value) {
		return this.eq(path(attribute1, attribute2), value);
	}

	default <X, Y, V> ISubquery<F, R> eq(final SingularAttribute<? super F, X> attribute1, final SingularAttribute<? super X, Y> attribute2, final SingularAttribute<? super Y, V> attribute3, final V value) {
		return this.eq(path(attribute1, attribute2, attribute3), value);
	}

	ISubquery<F, R> groupBy(IExpression<?>... expressions);

	ISubquery<F, R> having(IPredicate... predicates);

	javax.persistence.criteria.Subquery<R> create(IQueryDataHolder data);

	IExpression<R> getSelection();

	@Override
	default javax.persistence.criteria.Subquery<R> resolve(final IQueryDataHolder data) {
		return data.getSubquery(this, () -> this.create(data));
	}
}
