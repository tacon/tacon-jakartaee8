package dev.tacon.jakartaee8.persistence.query.criteria;

import javax.persistence.criteria.Path;
import javax.persistence.metamodel.MapAttribute;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.persistence.query.IQueryDataHolder;

@FunctionalInterface
public interface IPath<X> extends IExpression<X> {

	@Override
	Path<X> resolve(IQueryDataHolder data);

	default <Y> IPath<Y> get(final @NonNull SingularAttribute<? super X, Y> attribute) {
		return data -> this.resolve(data).get(attribute);
	}

	default <E, C extends java.util.Collection<E>> IExpression<C> get(final @NonNull PluralAttribute<X, C, E> attribute) {
		return data -> this.resolve(data).get(attribute);
	}

	default <K, V, M extends java.util.Map<K, V>> IExpression<M> get(final @NonNull MapAttribute<X, K, V> attribute) {
		return data -> this.resolve(data).get(attribute);
	}
}