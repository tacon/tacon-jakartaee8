package dev.tacon.jakartaee8.persistence.query.criteria;

@FunctionalInterface
public interface IRoot<X> extends IFrom<X, X> {
	//
}
