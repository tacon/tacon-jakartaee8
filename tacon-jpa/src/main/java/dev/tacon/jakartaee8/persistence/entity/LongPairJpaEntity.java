package dev.tacon.jakartaee8.persistence.entity;

import java.util.UUID;

public abstract class LongPairJpaEntity extends AbstractJpaEntity<LongPairPK> {

	public UUID getIdAsUUID() {
		final LongPairPK id = this.getId();
		return id != null ? id.getUUID() : null;
	}

	public void setIdAsUUID(final UUID uuid) {
		LongPairPK id = this.getId();
		if (id == null) {
			id = new LongPairPK();
			this.setId(id);
		}
		id.setUUID(uuid);
	}
}
