package dev.tacon.jakartaee8.persistence.entity;

import java.io.Serializable;
import java.util.Objects;

public abstract class AbstractJpaEntity<T extends Serializable> {

	public abstract T getId();

	public abstract void setId(T id);

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !obj.getClass().equals(this.getClass())) {
			return false;
		}
		return Objects.equals(this.getId(), ((AbstractJpaEntity<?>) obj).getId());
	}

	@Override
	public int hashCode() {
		final T id = this.getId();
		return id == null ? 0 : id.hashCode();
	}
}
