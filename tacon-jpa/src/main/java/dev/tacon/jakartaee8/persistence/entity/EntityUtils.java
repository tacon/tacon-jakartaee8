package dev.tacon.jakartaee8.persistence.entity;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Type.PersistenceType;

import dev.tacon.annotations.Nullable;

public final class EntityUtils {

	public static Set<Class<?>> getEntityClasses(final EntityManager entityManager) {
		return entityManager.getMetamodel().getManagedTypes()
				.stream().filter(managedType -> PersistenceType.ENTITY == managedType.getPersistenceType())
				.map(ManagedType::getJavaType)
				.collect(Collectors.toSet());
	}

	public static <T extends Serializable> T getId(final @Nullable AbstractJpaEntity<T> entity) {
		return entity != null ? entity.getId() : null;
	}

	public static UUID getId(final @Nullable LongPairJpaEntity entity) {
		return entity != null ? entity.getIdAsUUID() : null;
	}

	private EntityUtils() {}
}
