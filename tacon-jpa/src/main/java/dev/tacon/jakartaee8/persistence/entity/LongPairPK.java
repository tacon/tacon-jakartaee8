package dev.tacon.jakartaee8.persistence.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Embeddable;

import dev.tacon.annotations.NonNull;

@Embeddable
public class LongPairPK implements Serializable {

	private static final long serialVersionUID = -9067461681924649714L;

	private long mostSigBits;

	private long leastSigBits;

	public LongPairPK() {}

	public LongPairPK(final long mostSigBits, final long leastSigBits) {
		this.mostSigBits = mostSigBits;
		this.leastSigBits = leastSigBits;
	}

	public LongPairPK(final UUID uuid) {
		this.setUUID(uuid);
	}

	public long getMostSigBits() {
		return this.mostSigBits;
	}

	public void setMostSigBits(final long mostSigBits) {
		this.mostSigBits = mostSigBits;
	}

	public long getLeastSigBits() {
		return this.leastSigBits;
	}

	public void setLeastSigBits(final long leastSigBits) {
		this.leastSigBits = leastSigBits;
	}

	public UUID getUUID() {
		return new UUID(this.mostSigBits, this.leastSigBits);
	}

	public void setUUID(final @NonNull UUID uuid) {
		this.mostSigBits = uuid.getMostSignificantBits();
		this.leastSigBits = uuid.getLeastSignificantBits();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.mostSigBits ^ this.mostSigBits >>> 32);
		result = prime * result + (int) (this.leastSigBits ^ this.leastSigBits >>> 32);
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof LongPairPK)) {
			return false;
		}
		final LongPairPK other = (LongPairPK) obj;
		if (this.mostSigBits != other.mostSigBits
				|| this.leastSigBits != other.leastSigBits) {
			return false;
		}
		return true;
	}
}
