package dev.tacon.jakartaee8.validation;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class TemporalRangeValidatorForDate extends TemporalRangeValidator<Date, Instant> {

	@Override
	protected Instant convert(final Date value) {
		// java.sql.Date does not support toInstant()
		return Instant.ofEpochMilli(value.getTime());
	}

	@Override
	protected Instant getCurrent(final ChronoUnit unit) {
		return unit.isDateBased()
				? LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()
				: Instant.now();
	}

	@Override
	protected Instant calculateMin(final Instant current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected Instant calculateMax(final Instant current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
