package dev.tacon.jakartaee8.validation;

import java.time.temporal.ChronoUnit;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

abstract class TemporalRangeValidator<T, T1 extends Comparable<? super T1>> implements ConstraintValidator<TemporalRange, T> {

	private int backward;
	private int forward;
	private ChronoUnit temporalUnit;

	@Override
	public void initialize(final TemporalRange annotation) {
		this.backward = annotation.back();
		this.forward = annotation.forward();
		this.temporalUnit = annotation.unit();
	}

	@Override
	public boolean isValid(final T value, final ConstraintValidatorContext context) {
		if (value == null || this.backward < 0 && this.forward < 0) {
			return true;
		}

		final T1 convertedValue = this.convert(value);

		final T1 current = this.getCurrent(this.temporalUnit);

		if (this.backward >= 0) {
			final T1 minValue = this.calculateMin(current, this.backward, this.temporalUnit);
			if (minValue != null && convertedValue.compareTo(minValue) < 0) {
				return false;
			}
		}

		if (this.forward >= 0) {
			final T1 maxValue = this.calculateMax(current, this.forward, this.temporalUnit);
			if (maxValue != null && convertedValue.compareTo(maxValue) > 0) {
				return false;
			}
		}

		return true;
	}

	protected abstract T1 convert(final T value);

	protected abstract T1 getCurrent(final ChronoUnit unit);

	protected abstract T1 calculateMin(final T1 current, final int units, final ChronoUnit unit);

	protected abstract T1 calculateMax(final T1 current, final int units, final ChronoUnit unit);
}
