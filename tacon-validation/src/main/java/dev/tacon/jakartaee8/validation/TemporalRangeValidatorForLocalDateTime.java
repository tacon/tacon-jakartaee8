package dev.tacon.jakartaee8.validation;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForLocalDateTime extends TemporalRangeValidator<LocalDateTime, LocalDateTime> {

	@Override
	protected LocalDateTime convert(final LocalDateTime value) {
		return value;
	}

	@Override
	protected LocalDateTime getCurrent(final ChronoUnit unit) {
		return LocalDateTime.now();
	}

	@Override
	protected LocalDateTime calculateMin(final LocalDateTime current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected LocalDateTime calculateMax(final LocalDateTime current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}
