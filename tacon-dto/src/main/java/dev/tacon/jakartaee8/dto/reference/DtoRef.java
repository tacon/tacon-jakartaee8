package dev.tacon.jakartaee8.dto.reference;

import java.io.Serializable;
import java.util.Objects;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.Dto;

/**
 * @param <D>
 */
public class DtoRef<D extends Dto, I> implements Serializable {

	private static final long serialVersionUID = 2538378520796246528L;

	private I id;

	protected DtoRef() {}

	public DtoRef(final @NonNull I id) {
		this.id = Objects.requireNonNull(id, "ID cannot be null in a reference");
	}

	public I getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return this.id.toString();
	}

	@Override
	public int hashCode() {
		return this.hashCodeId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		return this.equalsId(((DtoRef<?, ?>) obj).id);
	}

	protected int hashCodeId() {
		return this.id.hashCode();
	}

	protected boolean equalsId(final Object other) {
		return this.id.equals(other);
	}
}
