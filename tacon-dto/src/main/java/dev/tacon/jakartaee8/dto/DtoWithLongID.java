package dev.tacon.jakartaee8.dto;

import dev.tacon.jakartaee8.dto.reference.DtoLongRef;

public class DtoWithLongID<D extends DtoWithLongID<D>> extends DtoWithID<D, Long, DtoLongRef<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	protected DtoWithLongID() {
		super(null);
	}

	protected DtoWithLongID(final long id) {
		this(Long.valueOf(id));
	}

	protected DtoWithLongID(final Long id) {
		super(new DtoLongRef<D>(id));
	}

	protected DtoWithLongID(final DtoLongRef<D> ref) {
		super(ref);
	}
}
