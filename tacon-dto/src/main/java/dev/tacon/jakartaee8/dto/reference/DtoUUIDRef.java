package dev.tacon.jakartaee8.dto.reference;

import java.util.UUID;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.DtoWithUUID;

public class DtoUUIDRef<D extends DtoWithUUID<D>> extends DtoRef<D, UUID> {

	private static final long serialVersionUID = 926635774339007814L;

	protected DtoUUIDRef() {
		super();
	}

	public DtoUUIDRef(final @NonNull UUID id) {
		super(id);
	}
}
