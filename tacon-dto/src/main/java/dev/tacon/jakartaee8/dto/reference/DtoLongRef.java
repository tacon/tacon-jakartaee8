package dev.tacon.jakartaee8.dto.reference;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.DtoWithLongID;

public class DtoLongRef<D extends DtoWithLongID<D>> extends DtoRef<D, Long> {

	private static final long serialVersionUID = -3652131357431256774L;

	protected DtoLongRef() {
		super();
	}

	public DtoLongRef(final @NonNull Long id) {
		super(id);
	}

	public DtoLongRef(final long id) {
		super(Long.valueOf(id));
	}
}
