package dev.tacon.jakartaee8.dto.ext;

public interface HasInsertId<T> {

	void setInsertId(T insertId);

	T getInsertId();

	// forces generic type to be the same of the dto key class
	T getId();
}
