package dev.tacon.jakartaee8.dto.exception;

public class ManagerConcurrencyException extends RuntimeException {

	private static final long serialVersionUID = -722273837586030163L;

	public ManagerConcurrencyException(final String message) {
		super(message);
	}
}
