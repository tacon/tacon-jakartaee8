package dev.tacon.jakartaee8.dto.reference;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.DtoWithStringID;

public class DtoStringRef<D extends DtoWithStringID<D>> extends DtoRef<D, String> {

	private static final long serialVersionUID = -3652131357431256774L;

	protected DtoStringRef() {
		super();
	}

	public DtoStringRef(final @NonNull String id) {
		super(id);
	}
}
