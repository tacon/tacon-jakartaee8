package dev.tacon.jakartaee8.dto;

import java.util.UUID;

import dev.tacon.jakartaee8.dto.reference.DtoUUIDRef;

public abstract class DtoWithUUID<D extends DtoWithUUID<D>> extends DtoWithID<D, UUID, DtoUUIDRef<D>> {

	private static final long serialVersionUID = -555695435670136550L;

	protected DtoWithUUID() {
		super(null);
	}

	protected DtoWithUUID(final UUID id) {
		super(new DtoUUIDRef<D>(id));
	}

	protected DtoWithUUID(final DtoUUIDRef<D> ref) {
		super(ref);
	}
}
