package dev.tacon.jakartaee8.dto;

import dev.tacon.jakartaee8.dto.reference.DtoIntRef;

public class DtoWithIntID<D extends DtoWithIntID<D>> extends DtoWithID<D, Integer, DtoIntRef<D>> {

	private static final long serialVersionUID = 7459469513673286979L;

	protected DtoWithIntID() {
		super(null);
	}

	protected DtoWithIntID(final int id) {
		this(Integer.valueOf(id));
	}

	protected DtoWithIntID(final Integer id) {
		super(new DtoIntRef<>(id));
	}

	protected DtoWithIntID(final DtoIntRef<D> ref) {
		super(ref);
	}
}
