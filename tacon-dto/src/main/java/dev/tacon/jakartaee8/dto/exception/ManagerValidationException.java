package dev.tacon.jakartaee8.dto.exception;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;

public class ManagerValidationException extends ManagerException {

	private static final long serialVersionUID = -593270702974639168L;

	public static final String MESSAGE_KEY = "dev.tacon.validation";

	private final List<ValidationError> validationErrors;

	public ManagerValidationException(final List<ValidationError> validationErrors) {
		super(MESSAGE_KEY);
		this.validationErrors = validationErrors;
	}

	public List<ValidationError> getValidationErrors() {
		return this.validationErrors;
	}

	public static class ValidationError implements Serializable {

		private static final long serialVersionUID = 6556720269128436119L;

		private final Annotation annotation;
		private final Serializable rootBean;
		private final Serializable invalidValue;
		private final boolean field;
		private final String propertyPath;
		private final String message;
		private final String messageKey;
		private final Map<String, Serializable> messageParams;

		public ValidationError(final Annotation annotation, final Serializable rootBean, final Serializable invalidValue, final boolean field, final String propertyPath, final String message, final String messageKey, final Map<String, Serializable> messageParams) {
			this.annotation = annotation;
			this.rootBean = rootBean;
			this.invalidValue = invalidValue;
			this.field = field;
			this.propertyPath = propertyPath;
			this.message = message;
			this.messageKey = messageKey;
			this.messageParams = messageParams;
		}

		public Annotation getAnnotation() {
			return this.annotation;
		}

		public Serializable getRootBean() {
			return this.rootBean;
		}

		public Serializable getInvalidValue() {
			return this.invalidValue;
		}

		public boolean isField() {
			return this.field;
		}

		public String getPropertyPath() {
			return this.propertyPath;
		}

		public String getMessage() {
			return this.message;
		}

		public String getMessageKey() {
			return this.messageKey;
		}

		public Map<String, Serializable> getMessageParams() {
			return this.messageParams;
		}
	}
}
