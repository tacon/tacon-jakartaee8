package dev.tacon.jakartaee8.dto;

import dev.tacon.jakartaee8.dto.reference.DtoStringRef;

public class DtoWithStringID<D extends DtoWithStringID<D>> extends DtoWithID<D, String, DtoStringRef<D>> {

	private static final long serialVersionUID = 951210959671517022L;

	protected DtoWithStringID() {
		super(null);
	}

	protected DtoWithStringID(final String id) {
		super(new DtoStringRef<D>(id));
	}

	protected DtoWithStringID(final DtoStringRef<D> ref) {
		super(ref);
	}
}
