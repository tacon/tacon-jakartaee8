package dev.tacon.jakartaee8.dto;

import java.io.Serializable;
import java.lang.reflect.Constructor;

import dev.tacon.annotations.NonNull;

/**
 * Data Transfer Object interface.
 */
public interface Dto extends Serializable {

	@SuppressWarnings("unchecked")
	static @NonNull <D extends Dto> D of(final @NonNull Class<D> dtoClass) {
		try {
			final Constructor<?> constr = dtoClass.getConstructor();
			return (D) constr.newInstance();
		} catch (final ReflectiveOperationException roex) {
			throw new RuntimeException("Cannot find an empty constructor for " + dtoClass, roex);
		}
	}
}
