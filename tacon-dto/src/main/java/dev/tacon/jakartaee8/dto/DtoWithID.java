package dev.tacon.jakartaee8.dto;

import java.util.Objects;

import dev.tacon.jakartaee8.dto.reference.DtoRef;

public class DtoWithID<D extends Dto, I, R extends DtoRef<D, I>> implements Dto {

	private static final long serialVersionUID = -3716820606644291269L;

	private R ref;

	protected DtoWithID() {}

	protected DtoWithID(final R ref) {
		this.ref = ref;
	}

	public R getRef() {
		return this.ref;
	}

	public I getId() {
		return this.ref == null ? null : this.ref.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		if (this.ref == null || ((DtoWithID<?, ?, ?>) obj).ref == null) {
			return false;
		}
		return Objects.equals(this.ref, ((DtoWithID<?, ?, ?>) obj).ref);
	}

	@Override
	public int hashCode() {
		return this.ref == null ? 0 : this.ref.hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ",ref=" + this.ref;
	}
}
