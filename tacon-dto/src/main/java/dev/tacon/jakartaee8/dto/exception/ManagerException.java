package dev.tacon.jakartaee8.dto.exception;

public class ManagerException extends Exception {

	private static final long serialVersionUID = 982944505921493130L;

	private final String messageKey;

	public ManagerException(final String messageKey) {
		super();
		this.messageKey = messageKey;
	}

	public ManagerException(final String messageKey, final String message, final Throwable cause) {
		super(message, cause);
		this.messageKey = messageKey;
	}

	public ManagerException(final String messageKey, final String message) {
		super(message);
		this.messageKey = messageKey;
	}

	public ManagerException(final String messageKey, final Throwable cause) {
		super(cause);
		this.messageKey = messageKey;
	}

	public String getMessageKey() {
		return this.messageKey;
	}
}
