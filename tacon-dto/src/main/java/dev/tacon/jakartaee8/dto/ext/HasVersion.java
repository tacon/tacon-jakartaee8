package dev.tacon.jakartaee8.dto.ext;

public interface HasVersion<V extends Comparable<? super V>> {

	V getVersion();

	void setVersion(V version);

}
