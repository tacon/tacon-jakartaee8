package dev.tacon.jakartaee8.dto.reference;

import dev.tacon.annotations.NonNull;
import dev.tacon.jakartaee8.dto.DtoWithIntID;

public class DtoIntRef<D extends DtoWithIntID<D>> extends DtoRef<D, Integer> {

	private static final long serialVersionUID = -7191658974183485229L;

	protected DtoIntRef() {
		super();
	}

	public DtoIntRef(final @NonNull Integer id) {
		super(id);
	}

	public DtoIntRef(final int id) {
		super(Integer.valueOf(id));
	}
}
