package dev.tacon.jakartaee8.dto;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee8.dto.reference.DtoIntRef;
import dev.tacon.jakartaee8.dto.reference.DtoLongRef;
import dev.tacon.jakartaee8.dto.reference.DtoRef;
import dev.tacon.jakartaee8.dto.reference.DtoStringRef;
import dev.tacon.jakartaee8.dto.reference.DtoUUIDRef;

public final class DtoUtils {

	public static <D extends DtoWithID<D, K, R>, K, R extends DtoRef<D, K>> Set<R> refs(final @Nullable Collection<D> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::ref).collect(Collectors.toSet()) : null;
	}

	public static <D extends DtoWithID<D, K, R>, K, R extends DtoRef<D, K>> Set<D> toDtos(final @Nullable Collection<R> refs, final @NonNull Function<R, D> toDtoFunction) {
		return refs != null ? refs.stream().map(toDtoFunction).collect(Collectors.toSet()) : null;
	}

	public static <D extends DtoWithID<D, K, R>, K, R extends DtoRef<D, K>> R ref(final @Nullable D dto) {
		return dto != null ? dto.getRef() : null;
	}

	public static <K> Set<K> refIds(final @Nullable Collection<? extends DtoRef<?, K>> refs) {
		return refs != null ? refs.stream().map(DtoUtils::refId).collect(Collectors.toSet()) : null;
	}

	public static <D extends DtoWithID<D, K, ?>, K> Set<K> ids(final @Nullable Collection<D> dtos) {
		return dtos != null ? dtos.stream().map(DtoUtils::id).collect(Collectors.toSet()) : null;
	}

	public static <K> K refId(final @Nullable DtoRef<?, K> ref) {
		return ref != null ? ref.getId() : null;
	}

	public static <D extends DtoWithID<D, K, ?>, K> K id(final @Nullable D dto) {
		return dto != null ? dto.getId() : null;
	}

	public static <D extends DtoWithUUID<D>> DtoUUIDRef<D> ref(final @Nullable UUID key) {
		return key != null ? new DtoUUIDRef<>(key) : null;
	}

	public static <D extends DtoWithStringID<D>> DtoStringRef<D> ref(final @Nullable String key) {
		return key != null ? new DtoStringRef<>(key) : null;
	}

	public static <D extends DtoWithIntID<D>> DtoIntRef<D> ref(final @Nullable Integer key) {
		return key != null ? new DtoIntRef<>(key) : null;
	}

	public static <D extends DtoWithLongID<D>> DtoLongRef<D> ref(final @Nullable Long key) {
		return key != null ? new DtoLongRef<>(key) : null;
	}

	private DtoUtils() {
		throw new AssertionError();
	}
}